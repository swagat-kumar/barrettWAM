/* TESTING of GWAM CLASS
 * Date: March 22, 2016
 *
 * Both types of Jacobian are tested
 *
 * Orientation Jacobian is tested through Simulation
 * Angular Velocity Jacobian is tested by Matching it with GWAM manipulator jacobian obtained from the system.
 *
 * ----------------------------------- */

#include <iostream>
#include <gwam_fk2.h>
#include<gnuplot_ci.h>

using namespace std;
using namespace gnuplot_ci;
using namespace GWAM_FK;

//#define JACOB_TEST
#define IK_TEST1


int main()
{
    GWAM gwam_robot;


#ifdef JACOB_TEST
    //double theta[NL] = {0, 0, DEG2RAD(30), DEG2RAD(60), 0, DEG2RAD(30), 0};
    double theta[NL] = {DEG2RAD(10), DEG2RAD(30), DEG2RAD(30), 0, DEG2RAD(30), DEG2RAD(20), DEG2RAD(-10)};
    //double theta[NL] = {0, DEG2RAD(0), DEG2RAD(0), 0, DEG2RAD(0), 0, 0};

    gwam_robot.set_joint_angles(theta);
    gwam_robot.position_jacobian();
    gwam_robot.orientation_jacobian();
    gwam_robot.linear_velocity_jacobian();
    gwam_robot.angular_velocity_jacobian();
    gwam_robot.display_variables();


#endif

#ifdef IK_TEST1

    int num = 2000;
    double angle[num][NL];

    double init_config[NL] = {0, 0, 0, 0, 0, 0, 0};
    double pref_config[NL] = {-0.0574878, 0.594913, 0.0453384, 1.16046,
            -0.201394, -0.0951226, -0.341729};

    double theta[NL] = {0, DEG2RAD(0), DEG2RAD(0), 0, DEG2RAD(0), 0, 0};

    double pose_t[] = {1.00007,	-0.328537,	0.575816,	1.28715,	0.516391,	0.791467};

    ofstream f1("target_pose.txt");
    ofstream f2("rconfig.txt");
    ofstream f3("actual_pose.txt");

    double err = gwam_robot.ik_traj_withpose(init_config,pose_t, pref_config,angle, num);


    cout << "error = " <<  err << endl;

    for(int i = 0; i < NW; ++i)
        f1 << pose_t[i] << "\t";
    f1 << endl;

    double pose[NW];
    cv::Mat Jpos(NR,3,CV_64F,0.0);
    for(int cnt = 0; cnt < num; ++cnt)
    {
        for(int i = 0; i < NL; ++i)
        {
            theta[i] = angle[cnt][i];
        }
        gwam_robot.set_joint_angles(theta);
        gwam_robot.joint_position(Jpos);
        gwam_robot.fwd_kin_pose();
        gwam_robot.get_ee_pose(pose);

        for(int i = 0; i < NR; ++i)
        {
            for(int j = 0; j < 3; ++j)
                f2 << Jpos.at<double>(i,j) << "\t";
            f2 << endl;
        }

        f2 << endl << endl;

        for(int i = 0; i < NW; ++i)
            f3 << pose[i] << "\t";
        f3 << endl;
    }


    f1.close();
    f2.close();
    f3.close();


    GP_handle G4("/usr/bin/", "X (m)", "Y (m)", "Z(m)");
    G4.gnuplot_cmd("set terminal wxt");
    G4.gnuplot_cmd("set border");
    G4.gnuplot_cmd("set ticslevel 0");
    G4.gnuplot_cmd("splot 'target_pose.txt' u 1:2:3 w p ps 2 pt 4 t 'target','actual_pose.txt' u 1:2:3 w p t 'actual'");
    G4.gnuplot_cmd("replot 'rconfig.txt' index 0:1900:10 u 1:2:3 w lp lw 2");
    //G4.draw3dcoordAxis(xorg, xb[0], xb[1], xb[2],true);

    cout << "Press Enter to exit .." << endl;
    getchar();

#endif





    return 0;

}
