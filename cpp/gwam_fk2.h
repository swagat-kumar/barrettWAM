#ifndef GWAM_FK2_H
#define GWAM_FK2_H

#include <cmath>
#include <fstream>
#include <opencv2/opencv.hpp>


namespace GWAM_FK
{
//Algorithm Settings
//#define PD_CONTROL    // use only one option at a time
//#define JTM
#define PI_NSO

#define DEG2RAD(x) M_PI*x/180.0
#define RAD2DEG(x) 180.0*x/M_PI

//Configuration Settings
#define NL 7   // Dimension of joint space
#define NW 6   // Dimension of workspace ( 3 - for only position, 6 - for pose = position + orientation)
#define NR 5
#define NC 3
#define dt 0.01 // Sampling time in Seconds
#define TMAX 20 // Total Simulation time in seconds



class GWAM
{
private:

    bool FLAG;

    // Only theta starts with index 1. Rest all starts with index 0.

    double theta[NL+1], pose[NW], posn[NC], rpy[NC];

    cv::Mat Jp, Jo, Jw, Jv, J, R;
    std::vector<double> theta_max, theta_min, c_min, c_max;


    //GWAM d-h parameters (Don't Change)
    const double a3 = 0.045, a4 = -0.045, d3 = 0.55, d5 = 0.3, d7 = 0.48;  // in meters

public:
    GWAM();
    void set_joint_angles(const double Th[]);
    void get_joint_angles(double Th[]);
    void get_ee_pose(double xp[]);
    void get_jacobian(cv::Mat &J);
    void display_variables();
    inline bool exist(const std::string& name);
    void fwd_kin_posn();
    void rotation_matrix();
    void fwd_kin_pose();
    void generate_data(double Uc[], double Th[]);
    void generate_pose_data(double Uc[], double Th[]);
    void joint_position(cv::Mat &X);
    void position_jacobian();
    void orientation_jacobian();
    void compute_jacobian();
    void angular_velocity_jacobian(); // testing
    void linear_velocity_jacobian();
    void compute_jacobian2(); //testing
    void rotated_axis(const cv::Mat &XP, const double rpy[], const cv::Mat &T, cv::Mat &XP2);
    double ik_traj_withpose(const double init_config[NL], double pose_t[NW], const double pref_config[NL], double jtangles[][NL], int num);
    //double ik_traj_withpose2(const double init_config[NL], double pose_t[NW], const double pref_config[NL], double jtangles[][NL], int num);
};


}
#endif // GWAM_FK2_H
