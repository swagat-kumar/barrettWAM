/* Jacobian Transpose Method For solving IK for a given Robot POSE (position + orientation)
 *
 * Robot: GWAM
 *
 * Date: January 29, 2016
 *
 * Status: Working ...
 *
 * -----------------------------------------
*/

#include<iostream>
#include<mobile_manip.h>
#include<fstream>
#include<gnuplot_ci.h>

using namespace std;
using namespace gnuplot_ci;






int main()
{

    cv::Mat Jpos(NR,3,CV_64F,0.0);
    cv::Mat Jpos_t(NR,3,CV_64F,0.0);
    cv::Mat Jpos_init(NR,3,CV_64F,0.0);

    //Desired Target Pose

    double theta_t[NL] = {DEG2RAD(30), DEG2RAD(50), DEG2RAD(20), DEG2RAD(50), DEG2RAD(10), DEG2RAD(20), DEG2RAD(0), DEG2RAD(10), 1,1};
    double pose_t[NW];
    bool VALID_FLAG;
    mmanip_pose_fk(theta_t, pose_t, VALID_FLAG);
    joint_position(theta_t, Jpos_t);

    // Initial values
    double pose[NW];
    double theta[NL] = {0,0,0,0,0,0,0,0,0,0};
    mmanip_pose_fk(theta, pose, VALID_FLAG);
    joint_position(theta, Jpos_init);

    ofstream f7("init_config.txt");
    for(int i = 0; i < NR; ++i)
    {
        for(int j = 0; j < NC; ++j)
            f7 << Jpos_init.at<double>(i,j) << "\t";
        for(int j = 0; j < NC; ++j)
            f7 << Jpos_t.at<double>(i,j) << "\t";
        f7 << endl;
    }
    f7.close();




    cv::Mat Pose_T(NW,1, CV_64F, pose_t);
    cv::Mat Pose_C(NW,1, CV_64F, pose);

    cv::Mat Jp(NC,NL,CV_64F, 0.0);
    cv::Mat Jw(3,NL,CV_64F, 0.0);
    cv::Mat J(NW,NL,CV_64F, 0.0);

    cv::Mat Theta_dot(NL,1,CV_64F, 0.0);
    cv::Mat Error(NW,1,CV_64F, 0.0);


    ofstream f1("actual.txt");
    ofstream f2("rconfig.txt");
    ofstream f5("error.txt");

    int Tmax = 100;
    double dt = 0.01;
    for(double t = 0; t < Tmax; t = t + dt)
    {
        // Pose Error
        Error = Pose_T - Pose_C;
        f5 << t << "\t" << sqrt(cv::Mat(Error.t()*Error).at<double>(0,0)/6.0)<< endl;

        //Jacobian
        position_jacobian(theta, Jp);
        orientation_jacobian(theta, Jw);
        cv::vconcat(Jp, Jw, J);


        //step size : alpha
        double alpha = cv::norm(J.t()*Error) / cv::norm(J*J.t()*Error);

        // Jacobian Transpose Method for computing theta_dot
        Theta_dot = alpha * J.t() * Error;


        for(int i = 0; i < NL; ++i)
            theta[i] = theta[i] + dt * Theta_dot.at<double>(i);

        mmanip_pose_fk(theta, pose, VALID_FLAG);
        joint_position(theta, Jpos);

        for(int i = 0; i < NR; ++i)
        {
            for(int j = 0; j < NC; ++j)
                f2 << Jpos.at<double>(i,j) << "\t";
            f2 << endl;
        }
        f2 << endl << endl;

        //update current pose

        if(VALID_FLAG)
        {
            for(int i = 0; i < NW; ++i)
            {
                Pose_C.at<double>(i) = pose[i];
                f1 << Pose_C.at<double>(i) << "\t";
            }
            f1 << endl;
        }


    } // time-loop
    f1.close();
    f2.close();

    f5.close();


    // Save the last pose and target pose.

    ofstream f6("final_config.txt");

    for(int i = 0; i < NR; ++i)
    {
        for(int j = 0; j < NC; ++j)
            f6 << Jpos.at<double>(i,j) << "\t";
        f6 << endl;
    }
    f6.close();

    double base1[NC], base2[NC], dbase[NC] = {0.2, 0.2, 0.2};
    base1[0] = Jpos_init.at<double>(0,0) - dbase[0]/2.0;
    base1[1] = Jpos_init.at<double>(0,1) - dbase[1]/2.0;
    base1[2] = Jpos_init.at<double>(0,2);

    base2[0] = Jpos.at<double>(0,0) -dbase[0]/2.0;
    base2[1] = Jpos.at<double>(0,1) -dbase[1]/2.0;
    base2[2] = Jpos.at<double>(0,2);


    //------------------
     ofstream f4("target.txt");
     for(int i = 0; i < NW; ++i)
         f4 << pose_t[i] << "\t";
     f4 << endl;

     f4.close();
     //--------------------------------
     // Coordinate Frames
     //-----------------------------------

     double dx = 0.3, dx_t = 0.2;
     double xorg[3] = {0,0,0};
     double xb[3][3] = {{dx,0,0}, {0,dx,0}, {0,0,dx}};  // Base Frame for actual pose
     double xb_t[3][3] = {{dx_t,0,0}, {0,dx_t,0}, {0,0,dx_t}}; // Base Frame for Target Pose


     double xend[3][3] = {{pose[0], pose[1], pose[2]},
                          {pose[0], pose[1], pose[2]},
                          {pose[0], pose[1], pose[2]}};

     double xend_t[3][3] = {{pose_t[0], pose_t[1], pose_t[2]},
                           {pose_t[0], pose_t[1], pose_t[2]},
                           {pose_t[0], pose_t[1], pose_t[2]}};


     double xeb[3][3], xeb_t[3][3];

     cv::Mat R(3,3,CV_64F,0.0);
     cv::Mat R_t(3,3,CV_64F,0.0); //Rotation matrix for target pose
     cv::Mat T(3,3,CV_64F, &xend);
     cv::Mat T_t(3,3,CV_64F, &xend_t); //Translation for target pose
     cv::Mat baseFrame(3,3,CV_64F, &xb);
     cv::Mat baseFrame_t(3,3,CV_64F, &xb_t); // Base Frame for target pose
     cv::Mat endFrame(3,3,CV_64F, 0.0);
     cv::Mat endFrame_t(3,3,CV_64F, 0.0); // End-effector Frame for Target Pose

     rotation_matrix(theta, R);
     rotation_matrix(theta_t, R_t);

     // output is a column matrix
     endFrame = R * baseFrame.t() + T.t(); // check the transpose
     endFrame_t = R_t * baseFrame_t.t() + T_t.t(); // check the transpose

     // make it a row matrix
     for(int i = 0; i < 3; ++i)
     {
         for(int j = 0; j < 3; ++j)
         {
             xeb[i][j] = endFrame.at<double>(j,i); // make it a row matrix
             xeb_t[i][j] = endFrame_t.at<double>(j,i); // make it a row
         }
     }



    // ---------------------
    // Plotting
    // -----------------


    GP_handle G1("/usr/bin/", "X (m)", "Y (m)", "Z (m)");
    G1.gnuplot_cmd("set terminal wxt");
    G1.gnuplot_cmd("set border");
    G1.gnuplot_cmd("splot 'actual.txt' u 1:2:3 w p t 'actual', 'target.txt' u 1:2:3 w p ps 4");



    GP_handle G2("/usr/bin/", "Time (s)", "Error");
    G2.gnuplot_cmd("set terminal wxt");
    G2.gnuplot_cmd("set border");
    G2.gnuplot_cmd("plot 'error.txt' u 1:2 w l t 'pose error'");





    GP_handle G4("/usr/bin/", "X(m)", "Y(m)", "Z(m)");
    G4.gnuplot_cmd("set terminal wxt");
    G4.gnuplot_cmd("set border");
    G4.gnuplot_cmd("set ticslevel 0");
    //G4.gnuplot_cmd("set yrange [-0.2:0.2]");
    G4.gnuplot_cmd("splot 'final_config.txt' u 1:2:3 w lp lw 2 t 'actual', 'init_config.txt' u 1:2:3 w lp t 'Init', 'init_config.txt' u 4:5:6 w lp t 'target', 'target.txt' u 1:2:3 w p ps 3 t 'target'");
    G4.gnuplot_cmd("replot 'actual.txt' u 1:2:3 w d t 'EE trajectory'");
    G4.draw3dcoordAxis(xorg, xb[0], xb[1], xb[2],true);
    G4.draw3dcoordAxis(xend[0], xeb[0], xeb[1], xeb[2], true, 1);
    G4.draw3dcoordAxis(xend_t[0], xeb_t[0], xeb_t[1], xeb_t[2], true, 2);
    G4.draw_cuboid_solid(base1,dbase, 3);
    cout << "Press Enter to proceed ..." << endl;
    getchar();
    G4.draw_cuboid_solid(base2,dbase, 1);


    getchar();



    return 0;
}
