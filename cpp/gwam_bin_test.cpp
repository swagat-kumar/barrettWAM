/* Inverse Kinematics of GWAM Robot ARM using PSeudo-Inverse and Jacobian Transpose Method
 *
 * Date: February 02, 2016
 *
 * Status: Working
 *
 *
 * -----------------------------------------
*/

#include<iostream>
#include<gwam_fk.h>
#include<fstream>
#include<gnuplot_ci.h>

using namespace std;
using namespace gnuplot_ci;


#define SINGLE_POINT


//==========================================================
int main()
{

    double pref_config[NL] = {-0.0574878, 0.594913, 0.0453384, 1.16046,
            -0.201394, -0.0951226, -0.341729};

    // Read the bin positions

    ifstream fin("/home/swagat/TCS/Programs/GIT-REPO/vmc/barrettWAM/cpp/data/bin_centers.txt");
    ifstream fin2("/home/swagat/TCS/Programs/GIT-REPO/vmc/barrettWAM/cpp/data/bin_desired_pose.txt");

    ofstream f1("target_pose.txt");
    ofstream f2("rconfig.txt");
    ofstream f3("actual_pose.txt");

#ifdef SINGLE_POINT

    bool VALID;
    int num = 2000;
    double angle[num][NL];

    double theta[NL];
    //double pose_t[] = {-0.1449, -0.2688, 2.1437, 1.28715, 0.516, 0.791};
    double pose_t[] = {1.00007,	-0.328537,	0.575816,	1.28715,	0.516391,	0.791467};
    //double pose_t[] = {1.00007,	-0.328537,	0.575816,	DEG2RAD(90), 0.0, 0.0};

     double err = gwam_ik_withpose(pose_t, pref_config,angle, num);

     cout << "error = " <<  err << endl;

     for(int i = 0; i < NW; ++i)
         f1 << pose_t[i] << "\t";
     f1 << endl;

     double pose[NW];
     cv::Mat Jpos(NR,3,CV_64F,0.0);
     for(int cnt = 0; cnt < num; ++cnt)
     {
         for(int i = 0; i < NL; ++i)
         {
             theta[i] = angle[cnt][i];
             cout << theta[i] << "\t";
         }
         cout << endl;
         joint_position(theta, Jpos);
         gwam_pose_fk(theta, pose, VALID);

         for(int i = 0; i < NR; ++i)
         {
             for(int j = 0; j < 3; ++j)
                 f2 << Jpos.at<double>(i,j) << "\t";
             f2 << endl;
         }

         f2 << endl << endl;

         for(int i = 0; i < NW; ++i)
             f3 << pose[i] << "\t";
         f3 << endl;

     }



#else


    int num = 5;
    double angle[num][NL];
    double theta[NL];
    for(int count = 0; count < 12; ++count) // Each row
    {

        double readval[NW];
        double pose1[NC], pose_t[NW];
        for(int j = 0; j < NC; ++j)
            fin >> pose_t[j];

        for(int j = 0; j < NW; ++j)
           fin2 >> readval[j];

        pose_t[3] = DEG2RAD(90); pose_t[4] = 0.0; pose_t[5] = 0;
        for(int j = 0; j < NW; ++j)
        {
//            if (j < NC)
//                pose_t[j] = pose1[j];
//            else
//                pose_t[j] = readval[j];

            f1 << pose_t[j] << "\t";
        }
        f1 << endl;

        double err = gwam_ik_withpose(pose_t, pref_config,angle, num);

        cout << count << "\t" << err << endl;

        bool VALID;
        double pose[NW];
        cv::Mat Jpos(NR,3,CV_64F,0.0);
        for(int i = 0; i < NL; ++i)
            theta[i] = angle[num-1][i];
        joint_position(theta, Jpos);
        gwam_pose_fk(theta, pose, VALID);

        for(int i = 0; i < NR; ++i)
        {
            for(int j = 0; j < 3; ++j)
                f2 << Jpos.at<double>(i,j) << "\t";
            f2 << endl;
        }
        f2 << endl << endl;

        for(int i = 0; i < NW; ++i)
            f3 << pose[i] << "\t";
        f3 << endl;

    }
#endif

    // ---------------------------------------------------------------
    f1.close();
    f2.close();
    f3.close();
    fin.close();
    fin2.close();


#ifdef SINGLE_POINT
    GP_handle G4("/usr/bin/", "X (m)", "Y (m)", "Z(m)");
    G4.gnuplot_cmd("set terminal wxt");
    G4.gnuplot_cmd("set border");
    G4.gnuplot_cmd("set ticslevel 0");
    G4.gnuplot_cmd("splot 'target_pose.txt' u 1:2:3 w p ps 2 pt 4 t 'target','actual_pose.txt' u 1:2:3 w p t 'actual'");
    G4.gnuplot_cmd("replot 'rconfig.txt' index 0:1900:10 u 1:2:3 w lp lw 2");
    //G4.draw3dcoordAxis(xorg, xb[0], xb[1], xb[2],true);

    cout << "Press Enter to exit .." << endl;
    getchar();

#else

    GP_handle G4("/usr/bin/", "X (m)", "Y (m)", "Z(m)");
    G4.gnuplot_cmd("set terminal wxt");
    G4.gnuplot_cmd("set border");
    G4.gnuplot_cmd("set ticslevel 0");
    G4.gnuplot_cmd("splot 'target_pose.txt' u 1:2:3 w p ps 2 pt 4 t 'target','actual_pose.txt' u 1:2:3 w p t 'actual'");
    G4.gnuplot_cmd("replot 'rconfig.txt' u 1:2:3 w lp lw 2");
    //G4.draw3dcoordAxis(xorg, xb[0], xb[1], xb[2],true);

    cout << "Press Enter to exit .." << endl;
    getchar();


#endif

    return 0;
}
