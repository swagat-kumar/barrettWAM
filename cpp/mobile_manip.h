#ifndef _GWAM_
#define _GWAM_

#include <cmath>
#include <fstream>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

#define DEG2RAD(x) M_PI*x/180.0
#define RAD2DEG(x) 180.0*x/M_PI

#define NL 10  // Dimension of joint space (7 Joint angles, base rotation angle, base_x, base_y)
#define NW 6   // Dimension of workspace ( 3 - for only position, 6 - for pose = position + orientation)
#define NC 3   // Dimension of Cartesian workspace (only position)
#define NR 7   // No. of locations in the joint config



// Range of Configuration space
const double theta_max[NL] = {2.6, 2.0, 2.8, 3.1, 1.24, 1.6, 3.0, 3.1, 0.8, 2.0};
const double theta_min[NL] = {-2.6, -2.0, -2.8, -0.9, -4.76, -1.6, -3.0, -3.1, -2.0, -2.0};


// Range of Workspace in Cartesian Space
const double c_min[3] = {-0.8, -0.8, -0.5};
const double c_max[3] = {0.8, 0.8, 0.8};

// pitch (beta) range: -1.5 to 1.5
// yaw (alpha) range: -3.14 to 3.14
// roll (gamma) range: -3.14 to 3.14

//GWAM d-h parameters
const double a3 = 0.045, a4 = -0.045, d3 = 0.55, d5 = 0.3, d7 = 0.48;  // in meters
const double db = 0.8;

inline bool exist(const std::string& name);
void mmanip_7dof_FK(const double Th[], double x[]);
void rotation_matrix(const double Th[], Mat &R);
void mmanip_pose_fk(double Th[], double xp[], bool & VALID);
void generate_data(double Uc[], double Th[]);
void generate_pose_data(double Uc[], double Th[]);
void joint_position(const double theta[7], Mat &X);
void position_jacobian(double Th[], cv::Mat &Jp);
void orientation_jacobian(double Th[], cv::Mat &Jw);
void rotated_axis(const cv::Mat &XP, const double rpy[], const cv::Mat &T, cv::Mat &XP2);
void angular_jacobian(double Th[], cv::Mat &Jw);

void get_euler_angles(cv::Mat &R, double xp[]);

#endif
