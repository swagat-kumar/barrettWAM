#ifndef _GWAM_
#define _GWAM_

#include <cmath>
#include <fstream>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

#define DEG2RAD(x) M_PI*x/180.0
#define RAD2DEG(x) 180.0*x/M_PI


//Configuration Settings
#define NL 7   // Dimension of joint space
#define NW 6   // Dimension of workspace ( 3 - for only position, 6 - for pose = position + orientation)
#define NR 5
#define NC 3
#define dt 0.01 // Sampling time in Seconds
#define TMAX 20 // Total Simulation time in seconds

//Algorithm Settings
//#define PD_CONTROL    // use only one option at a time
//#define JTM
#define PI_NSO


// Range of Configuration space
const double theta_max[7] = {2.6, 2.0, 2.8, 3.1, 1.24, 1.6, 3.0};
const double theta_min[7] = {-2.6, -2.0, -2.8, -0.9, -4.76, -1.6, -3.0};
//double theta_t[NL] =        {0, 1.0, 0, 0.8, -0.0, 0, 0};
// Range of Workspace in Cartesian Space
//#if NW == 3
const double c_min[3] = {-0.8, -0.8, -0.5};
const double c_max[3] = {0.8, 0.8, 0.8};
//#elif NW == 6  // how to define the limit for orientation
//const double c_min[6] = {0, -0.4, 0, -1.5, -3.14, -3.14};  //position and orientation (pitch-yaw-roll)
//const double c_max[6] = {0.45, 0.4, 0.6, 1.5, 3.14, 3.14}; //position and orientation (pitch-yaw-roll)
//#endif

// pitch (beta) range: -1.5 to 1.5
// yaw (alpha) range: -3.14 to 3.14
// roll (gamma) range: -3.14 to 3.14

//GWAM d-h parameters
const double a3 = 0.045, a4 = -0.045, d3 = 0.55, d5 = 0.3, d7 = 0.48;  // in meters

inline bool exist(const std::string& name);
void gwam_7dof_FK(const double Th[], double x[]);
void rotation_matrix(const double Th[], Mat &R);
void gwam_pose_fk(double Th[], double xp[], bool & VALID);
void generate_data(double Uc[], double Th[]);
void generate_pose_data(double Uc[], double Th[]);
void joint_position(const double theta[7], Mat &X);
void position_jacobian(double Th[], cv::Mat &Jp);
void orientation_jacobian(double Th[], cv::Mat &Jw);
void rotated_axis(const cv::Mat &XP, const double rpy[], const cv::Mat &T, cv::Mat &XP2);
void angular_jacobian(double Th[], cv::Mat &Jw);
void position_jacobian2(const double Th[], cv::Mat &Jv);
double gwam_ik_withpose(double pose_t[NW], double pref_config[NL], double jtangles[][NL], int num);

#endif
