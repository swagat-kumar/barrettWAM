/* Pseudo-inverse based Inverse Kinematic Solution
 * Use Null-space joint velocity for JOINT Limit Avoidance (JLA)
 *
 * Method I:
 * \dot{q} = pinv(J) * (\dot{x}+ Kp * error) + (I-pinv(J)*J)\dot{q}_0
 *
 * Method II:
 * \dot{q} = pinv(J) * error + (I-pinv(J)*J)\dot{q}_0
 *
 * \dot{q}_0 = alpha * dW/dq
 * where W(q) avoid joint limits
 *
 * Robot: GWAM from Barret
 *
 * Date: February 01, 2016
 *
 * Status: Both methods I and II are working perfectly. I tried several points. Change the target pose by changing theta_t variable.
 *
 *
 * NOTE: It is important to note that one has to provide the complete pose (6x1) for the desired location.
 * We take the desired position as obtained from a Kinect mounted on the robot.
 * The orientation is obtained from a preferred robot pose for that position.
 * -----------------------------------------
*/

#include<iostream>
#include<mobile_manip.h>
#include<fstream>
#include<gnuplot_ci.h>

using namespace std;
using namespace gnuplot_ci;

//#define POSE_GEN
#define BIN_ALL
#define PLOT

int main()
{

#ifdef POSE_GEN
    ifstream f20("/home/swagat/TCS/Programs/GIT-REPO/vmc/barrettWAM/cpp/data/bin_angle.txt");
    ofstream f21("bin_desired_pose.txt");
    for(int i = 0; i < 12; ++i)
    {
        double jtangle[NL];
        for(int j = 0; j < NL; ++j)
            f20 >> jtangle[j];
        jtangle[7] = 0.0; jtangle[8] = 0.0; jtangle[9] = 0.0;

        double pose_t[NW];
        bool VALID_FLAG;

//        for(int j = 0; j < NL; ++j)
//            cout << jtangle[j] << "\t";
//        cout << endl;

        mmanip_pose_fk(jtangle, pose_t, VALID_FLAG);

        for(int j = 0; j < NW; ++j)
            f21 << pose_t[j] << "\t";
        f21 << endl;
    }

    f20.close();
    f21.close();
#endif

#ifdef BIN_ALL

    //--------------------------------------------
    //Initial Values
    double pose[NW];
    double theta[NL] = {0,0,0,0,0,0,0,0,0,0};
    bool VALID_FLAG;
    mmanip_pose_fk(theta, pose, VALID_FLAG);

    cv::Mat Pose_C(6,1, CV_64F, pose);

    //Initial Robot Config
    cv::Mat Jpos(NR,3,CV_64F,0.0);
    joint_position(theta, Jpos);

    ofstream f9("init_config.txt");
    for(int i = 0; i < NR; ++i)
    {
        for(int j = 0; j < 3; ++j)
            f9 << Jpos.at<double>(i,j) << "\t";
        f9 << endl;
    }
    f9.close();

    // Gains
    cv::Mat Kp(NW,NW,CV_64F,0.0);
    Kp = cv::Mat::eye(Kp.rows, Kp.cols, CV_64F);


    //-------------------------------------
    // base coordinate frame
    double dx = 0.3, dx_t = 0.2;
    double xorg[3] = {0,0,0};
    double xb[3][3] = {{xorg[0]+dx,0,0}, {0,xorg[1]+dx,0}, {0,0,xorg[2]+dx} };



    //---------------------------------------------------


    // Read the bin positions
    ifstream fin("/home/swagat/TCS/Programs/GIT-REPO/vmc/barrettWAM/cpp/data/bin_centers.txt");
    ifstream fin2("bin_desired_pose.txt");
    ofstream f1("target_pose.txt");
    ofstream f2("error.txt");
    ofstream f3("rconfig.txt");
    ofstream f4("actual_pose.txt");
    ofstream f5("final_angles.txt");
    ofstream f6("coord_frames.txt");
    ofstream f7("trajectories.txt");


    double mean_pose_error = 0.0;
    for(int count = 0; count < 12; ++count) // Each row
    {
        double readval[NW];
        double pose1[NC], pose_t[NW];
        for(int j = 0; j < NC; ++j)
            fin >> pose1[j];
        pose1[2] += 0.8; // increase base height

        for(int j = 0; j < NW; ++j)
           fin2 >> readval[j];

        for(int j = 0; j < NW; ++j)
        {
            if (j < NC)
                pose_t[j] = pose1[j];
            else
                pose_t[j] = readval[j];

            f1 << pose_t[j] << "\t";
        }
        f1 << endl;
        //---------------------------------

        cv::Mat Pose_T(6,1, CV_64F, pose_t);
        cv::Mat Pose_T_dot(NW,1, CV_64F, 0.0);
        cv::Mat Jp(NC,NL,CV_64F, 0.0);
        cv::Mat Jw(3,NL,CV_64F, 0.0);
        cv::Mat J(NW,NL,CV_64F, 0.0);
        cv::Mat Theta_dot(NL,1,CV_64F, 0.0);
        cv::Mat Error(NW,1,CV_64F, 0.0);
        cv::Mat Jpinv(NL,NW, CV_64F, 0.0);
        cv::Mat q0_dot(NL,1,CV_64F,0.0); // Self-motion velocity



        //-------------------------------------------
        // Control Loop
        //----------------------------------------------

        int Tmax = 20;
        double dt = 0.001;
        for(double t = 0; t < Tmax; t = t + dt)
        {
            Error = Pose_T - Pose_C;

            //Jacobian
            position_jacobian(theta, Jp);
            orientation_jacobian(theta, Jw);
            cv::vconcat(Jp, Jw, J);

            cv::invert(J, Jpinv, cv::DECOMP_SVD);


            // Here w(q) = 1/n* \sum_i{(theta[i]/(theta_max[i] - theta_min[i]))^2}
            // dw/dq_i = theta[i];
            // null space optimization for obtained a desired joint configuration
            for(int i = 0; i < NL; ++i)
            {
                q0_dot.at<double>(i,0) = 2*theta[i] / (NL * pow((theta_max[i] - theta_min[i]),2.0));
            }


            // Joint angle velocity using Self-motion component
            // pseudoinverse solution with nullspace optimization
            //Theta_dot = Jpinv * (Pose_T_dot + Kp*Error) - 1.5*(cv::Mat::eye(NL,NL,CV_64F)-Jpinv*J) * q0_dot;


            // Pseudo-inverse with Null Space Optimzation (NSO) = PI+NSO
            Theta_dot = Jpinv * Error - 1.0 * (cv::Mat::eye(NL,NL,CV_64F)-Jpinv*J) * q0_dot;

            for(int i = 0; i < NL; ++i)
            {
                theta[i] = theta[i] + dt * Theta_dot.at<double>(i);

                if(theta[i] > theta_max[i]) theta[i] = theta_max[i];
                else if(theta[i] < theta_min[i]) theta[i] = theta_min[i];


            }

//            if(theta[8] > theta_max[8]) theta[8] = theta_max[8];
//            else if(theta[8] < theta_min[8]) theta[8] = theta_min[8];

//            if(theta[9] > theta_max[9]) theta[9] = theta_max[9];
//            else if(theta[9] < theta_min[9]) theta[9] = theta_min[9];

            //update current pose
            mmanip_pose_fk(theta, pose, VALID_FLAG);

            if(VALID_FLAG)
            {
                for(int i = 0; i < NW; ++i)
                    Pose_C.at<double>(i) = pose[i];

            }
        }//time-control-loop

        //Final robot pose for the current bin
        for(int i = 0; i < NW; ++i)
            f4 << pose[i] << "\t";
        f4 << endl;


        // final pose for the current bin
        joint_position(theta, Jpos);

        for(int i = 0; i < NR; ++i)
        {
            for(int j = 0; j < 3; ++j)
                f3 << Jpos.at<double>(i,j) << "\t";
            f3 << endl;
        }
        f3 << endl << endl;


        mean_pose_error =+ cv::Mat(Error.t()*Error).at<double>(0,0)/6.0;

        f2 << count << "\t" << sqrt(cv::Mat(Error.t()*Error).at<double>(0,0)/NW)<< "\t" << sqrt(mean_pose_error/count) << endl;



        cout << count << "\t" << sqrt(cv::Mat(Error.t()*Error).at<double>(0,0)/NW) << endl;



        for(int i = 0; i < NL; ++i)
            f5 << theta[i] << "\t";
        f5 << endl;


        //---------------------------
        // Coordinate Frames
        //---------------------------


        double xend[3][3] = {{pose[0], pose[1], pose[2]},
                             {pose[0], pose[1], pose[2]},
                             {pose[0], pose[1], pose[2]}};

        double xeb[3][3];

        cv::Mat R(3,3,CV_64F,0.0); // rotation matrix for final theta
        cv::Mat T(3,3,CV_64F, &xend);
        cv::Mat baseFrame(3,3,CV_64F, &xb);
        cv::Mat endFrame(3,3,CV_64F, 0.0);

        rotation_matrix(theta, R);


        // output is a column matrix
        endFrame = R * baseFrame.t() + T.t(); // check the transpose

        // make it a row matrix
        for(int i = 0; i < 3; ++i)
        {
            for(int j = 0; j < 3; ++j)
                f6 << endFrame.at<double>(j,i) << "\t";
            f6 << endl;
        }
        f6 << endl << endl;

    } // For each bin location

    f1.close();
    f2.close();
    f3.close();
    f4.close();
    fin.close();
    fin2.close();
    f5.close();
    f6.close();

    // ---------------------------------------------------------------

#endif

#ifdef PLOT



    // ---------------------
    // Plotting
    // -----------------




    GP_handle G4("/usr/bin/", "X (m)", "Y (m)", "Z(m)");
    G4.gnuplot_cmd("set terminal wxt");
    G4.gnuplot_cmd("set border");
    G4.gnuplot_cmd("set ticslevel 0");
    G4.gnuplot_cmd("splot 'target_pose.txt' u 1:2:3 w p ps 2 pt 4 t 'target', 'actual_pose.txt' u 1:2:3 w p t 'actual'");
    G4.gnuplot_cmd("replot 'rconfig.txt' u 1:2:3 w lp lw 2, 'init_config.txt' u 1:2:3 w lp t 'Init'");
    G4.draw3dcoordAxis(xorg, xb[0], xb[1], xb[2],true);




    cout << "Press Enter to exit .." << endl;
    getchar();

#endif


    return 0;
}
