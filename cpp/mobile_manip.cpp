#include <mobile_manip.h>

//----------------------------
inline bool exist(const std::string& name)
{
    std::ifstream file(name.c_str());
    if(!file)            // If the file was not found, then file is 0, i.e. !file=1 or true.
        return false;    // The file was not found.
    else                 // If the file was found, then file is non-0.
        return true;     // The file was found.
}
//-------------------------------

//--------------------------------------------------------
// Forward kinematics
// Th - Joint angle values in radians
// x - Cartesian position of end-effector in meters
void mmanip_7dof_FK(const double Th[], double eeposn[])
{


  double theta[9], x, y;
  theta[1] = Th[0];
  theta[2] = Th[1];
  theta[3] = Th[2];
  theta[4] = Th[3];
  theta[5] = Th[4];
  theta[6] = Th[5];
  theta[7] = Th[6];
  theta[8] = Th[7];
  x = Th[8];
  y = Th[9];



  eeposn[0] = x-sin(theta[8])*(sin(theta[1])*(cos(theta[2])*(cos(theta[3])*
          (sin(theta[4])*(cos(theta[6])*d7+d5)+cos(theta[4])*cos(theta[5])*sin(theta[6])*d7+cos(theta[4])*a4)-sin(theta[3])*sin(theta[5])*sin(theta[6])*d7+
          cos(theta[3])*a3)+sin(theta[2])*(cos(theta[4])*(cos(theta[6])*d7+d5)-sin(theta[4])*cos(theta[5])*sin(theta[6])*d7+d3-sin(theta[4])*a4))+cos(theta[1])*(
          sin(theta[3])*(sin(theta[4])*(cos(theta[6])*d7+d5)+cos(theta[4])*cos(theta[5])*sin(theta[6])*d7+cos(theta[4])*a4)+cos(theta[3])*sin(theta[5])*sin(theta[6])
          *d7+sin(theta[3])*a3))+cos(theta[8])*(cos(theta[1])*(cos(theta[2])*(cos(theta[3])*
          (sin(theta[4])*(cos(theta[6])*d7+d5)+cos(theta[4])*cos(theta[5])*sin(theta[6])*d7+cos(theta[4])*a4)-sin(theta[3])*sin(theta[5])*sin(theta[6])*d7+
          cos(theta[3])*a3)+sin(theta[2])*(cos(theta[4])*(cos(theta[6])*d7+d5)-sin(theta[4])*cos(theta[5])*sin(theta[6])*d7+d3-sin(theta[4])*a4))-sin(theta[1])*(
          sin(theta[3])*(sin(theta[4])*(cos(theta[6])*d7+d5)+cos(theta[4])*cos(theta[5])*sin(theta[6])*d7+cos(theta[4])*a4)+cos(theta[3])*sin(theta[5])*sin(theta[6])
          *d7+sin(theta[3])*a3));

  eeposn[1] = y+cos(theta[8])*(sin(theta[1])*(cos(theta[2])*(cos(theta[3])*
          (sin(theta[4])*(cos(theta[6])*d7+d5)+cos(theta[4])*cos(theta[5])*sin(theta[6])*d7+cos(theta[4])*a4)-sin(theta[3])*sin(theta[5])*sin(theta[6])*d7+
          cos(theta[3])*a3)+sin(theta[2])*(cos(theta[4])*(cos(theta[6])*d7+d5)-sin(theta[4])*cos(theta[5])*sin(theta[6])*d7+d3-sin(theta[4])*a4))+cos(theta[1])*(
          sin(theta[3])*(sin(theta[4])*(cos(theta[6])*d7+d5)+cos(theta[4])*cos(theta[5])*sin(theta[6])*d7+cos(theta[4])*a4)+cos(theta[3])*sin(theta[5])*sin(theta[6])
          *d7+sin(theta[3])*a3))+sin(theta[8])*(cos(theta[1])*(cos(theta[2])*(cos(theta[3])*
          (sin(theta[4])*(cos(theta[6])*d7+d5)+cos(theta[4])*cos(theta[5])*sin(theta[6])*d7+cos(theta[4])*a4)-sin(theta[3])*sin(theta[5])*sin(theta[6])*d7+
          cos(theta[3])*a3)+sin(theta[2])*(cos(theta[4])*(cos(theta[6])*d7+d5)-sin(theta[4])*cos(theta[5])*sin(theta[6])*d7+d3-sin(theta[4])*a4))-sin(theta[1])*(
          sin(theta[3])*(sin(theta[4])*(cos(theta[6])*d7+d5)+cos(theta[4])*cos(theta[5])*sin(theta[6])*d7+cos(theta[4])*a4)+cos(theta[3])*sin(theta[5])*sin(theta[6])
          *d7+sin(theta[3])*a3));


  eeposn[2] = db-sin(theta[2])*(cos(theta[3])*(sin(theta[4])*(cos(theta[6])*d7+d5)+cos(theta[4])*cos(theta[5])*sin(theta[6])*d7+cos(theta[4])*a4)-
              sin(theta[3])*sin(theta[5])*sin(theta[6])*d7+cos(theta[3])*a3)+cos(theta[2])*
              (cos(theta[4])*(cos(theta[6])*d7+d5)-sin(theta[4])*cos(theta[5])*sin(theta[6])*d7+d3-sin(theta[4])*a4);
}
//====================================================
// Computes the 3x3 rotation matrix for the end-effector coordinate system wrt the base frame
// Input: joint angle vector: Th[7]
// Output: Roatation matrix: R[3][3]
//----------------------------------------------


//-----------------------------------------------
// Gives the position of various joints
// theta[7] is in radians
// X should have 5 rows for GWAM
void joint_position(const double theta[], Mat &X)
{

    double th_1, th_2, th_3, th_4, th_5, th_6, th_7, th_8, x, y;

    th_1 = theta[0];
    th_2 = theta[1];
    th_3 = theta[2];
    th_4 = theta[3];
    th_5 = theta[4];
    th_6 = theta[5];
    th_7 = theta[6];
    th_8 = theta[7];
    x = theta[8];
    y = theta[9];


double t1[4][4] = { {cos(th_1), 0, -sin(th_1), 0},
                    {sin(th_1), 0, cos(th_1), 0},
                    {0, -1, 0, 0}, {0, 0, 0, 1}};

double t2[4][4] = { {cos(th_2), 0, sin(th_2),  0}, {sin(th_2), 0,
    -cos(th_2), 0}, {0, 1, 0, 0}, {0, 0, 0, 1} };

double t3[4][4] = { {cos(th_3), 0, -sin(th_3), a3*cos(th_3)},
    {sin(th_3), 0, cos(th_3), a3*sin(th_3)}, {0, -1, 0,
    d3}, {0, 0, 0, 1}};

double t4[4][4] = {{cos(th_4), 0, sin(th_4), a4*cos(th_4)},
    {sin(th_4), 0, -cos(th_4), a4*sin(th_4)}, {0, 1, 0,
    0},{0, 0, 0, 1}};

double t5[4][4] = { { cos(th_5), 0, -sin(th_5), 0},
    {sin(th_5), 0, cos(th_5), 0}, {0, -1, 0,
    d5}, {0, 0, 0, 1}} ;

double t6[4][4] = { { cos(th_6), 0, sin(th_6), 0}, {sin(th_6), 0,
    -cos(th_6), 0}, {0, 1, 0, 0}, {0, 0, 0, 1} };

double t7[4][4] = { {cos(th_7), -sin(th_7), 0, 0}, {sin(th_7),
    cos(th_7), 0, 0}, {0, 0, 1, d7}, {0, 0, 0, 1}};

double gtr[4][4] = { {1, 0, 0, x}, {0, 1, 0, y}, {0, 0, 1, 0}, {0, 0, 0, 1} };

double rtm[4][4] = { {cos(th_8), -sin(th_8), 0, 0},
                     {sin(th_8), cos(th_8), 0, 0},
                     {0, 0, 1, db}, {0, 0, 0, 1} };

    Mat T1 = Mat(4, 4, CV_64F, t1);
    Mat T2 = Mat(4, 4, CV_64F, t2);
    Mat T3 = Mat(4, 4, CV_64F, t3);
    Mat T4 = Mat(4, 4, CV_64F, t4);
    Mat T5 = Mat(4, 4, CV_64F, t5);
    Mat T6 = Mat(4, 4, CV_64F, t6);
    Mat T7 = Mat(4, 4, CV_64F, t7);
    Mat GTR = Mat(4, 4, CV_64F, gtr);
    Mat RTM = Mat(4,4, CV_64F, rtm);



    //Mat X = Mat(6, 3,CV_64F, 0.0);


    X.row(0) = GTR.col(3).rowRange(0,3).t();

    Mat TF0 = GTR * RTM;
    X.row(1) = TF0.col(3).rowRange(0,3).t();


    Mat TF1 = TF0 * T1 * T2;
    X.row(2) = TF1.col(3).rowRange(0,3).t();


    Mat TF2 = TF1*T3;
    X.row(3) = TF2.col(3).rowRange(0,3).t();


    Mat TF3 = TF2*T4;
    X.row(4) = TF3.col(3).rowRange(0,3).t();


    Mat TF4 = TF3*T5*T6;
    X.row(5) = TF4.col(3).rowRange(0,3).t();

    Mat TF5 = TF4*T7;
    X.row(6) = TF5.col(3).rowRange(0,3).t();

    //cout << X << endl;

}
//=============================================================

void rotation_matrix(const double Th[], Mat &R)
{


    double theta[9], x, y;
    theta[1] = Th[0];
    theta[2] = Th[1];
    theta[3] = Th[2];
    theta[4] = Th[3];
    theta[5] = Th[4];
    theta[6] = Th[5];
    theta[7] = Th[6];
    theta[8] = Th[7];
    x = Th[8];
    y = Th[9];


    R.at<double>(0,0) = (cos(theta[1])*(cos(theta[2])*(cos(theta[3])*
            (cos(theta[4])*(cos(theta[5])*cos(theta[6])*cos(theta[7])-sin(theta[5])*sin(theta[7]))-sin(theta[4])*sin(theta[6])*cos(theta[7]))-sin(theta[3])*
            (cos(theta[5])*sin(theta[7])+sin(theta[5])*cos(theta[6])*cos(theta[7])))+sin(theta[2])*
            (-sin(theta[4])*(cos(theta[5])*cos(theta[6])*cos(theta[7])-sin(theta[5])*sin(theta[7]))-cos(theta[4])*sin(theta[6])*cos(theta[7])))-sin(theta[1])*(
            sin(theta[3])*(cos(theta[4])*(cos(theta[5])*cos(theta[6])*cos(theta[7])-sin(theta[5])*sin(theta[7]))-sin(theta[4])*sin(theta[6])*cos(theta[7]))+cos(theta[3])*
            (cos(theta[5])*sin(theta[7])+sin(theta[5])*cos(theta[6])*cos(theta[7]))))*cos(theta[8])-(sin(theta[1])*(cos(theta[2])*(cos(theta[3])*
            (cos(theta[4])*(cos(theta[5])*cos(theta[6])*cos(theta[7])-sin(theta[5])*sin(theta[7]))-sin(theta[4])*sin(theta[6])*cos(theta[7]))-sin(theta[3])*
            (cos(theta[5])*sin(theta[7])+sin(theta[5])*cos(theta[6])*cos(theta[7])))+sin(theta[2])*
            (-sin(theta[4])*(cos(theta[5])*cos(theta[6])*cos(theta[7])-sin(theta[5])*sin(theta[7]))-cos(theta[4])*sin(theta[6])*cos(theta[7])))+cos(theta[1])*(
            sin(theta[3])*(cos(theta[4])*(cos(theta[5])*cos(theta[6])*cos(theta[7])-sin(theta[5])*sin(theta[7]))-sin(theta[4])*sin(theta[6])*cos(theta[7]))+cos(theta[3])*
            (cos(theta[5])*sin(theta[7])+sin(theta[5])*cos(theta[6])*cos(theta[7]))))*sin(theta[8]);


    R.at<double>(0,1) = (cos(theta[1])*(cos(theta[2])*(cos(theta[3])*
            (cos(theta[4])*(-cos(theta[5])*cos(theta[6])*sin(theta[7])-sin(theta[5])*cos(theta[7]))+sin(theta[4])*sin(theta[6])*sin(theta[7]))-sin(theta[3])*
            (cos(theta[5])*cos(theta[7])-sin(theta[5])*cos(theta[6])*sin(theta[7])))+sin(theta[2])*
            (cos(theta[4])*sin(theta[6])*sin(theta[7])-sin(theta[4])*(-cos(theta[5])*cos(theta[6])*sin(theta[7])-sin(theta[5])*cos(theta[7]))))-sin(theta[1])*(
            sin(theta[3])*(cos(theta[4])*(-cos(theta[5])*cos(theta[6])*sin(theta[7])-sin(theta[5])*cos(theta[7]))+sin(theta[4])*sin(theta[6])*sin(theta[7]))+
            cos(theta[3])*(cos(theta[5])*cos(theta[7])-sin(theta[5])*cos(theta[6])*sin(theta[7]))))*cos(theta[8])-(sin(theta[1])*(cos(theta[2])*(cos(theta[3])*
            (cos(theta[4])*(-cos(theta[5])*cos(theta[6])*sin(theta[7])-sin(theta[5])*cos(theta[7]))+sin(theta[4])*sin(theta[6])*sin(theta[7]))-sin(theta[3])*
            (cos(theta[5])*cos(theta[7])-sin(theta[5])*cos(theta[6])*sin(theta[7])))+sin(theta[2])*
            (cos(theta[4])*sin(theta[6])*sin(theta[7])-sin(theta[4])*(-cos(theta[5])*cos(theta[6])*sin(theta[7])-sin(theta[5])*cos(theta[7]))))+cos(theta[1])*(
            sin(theta[3])*(cos(theta[4])*(-cos(theta[5])*cos(theta[6])*sin(theta[7])-sin(theta[5])*cos(theta[7]))+sin(theta[4])*sin(theta[6])*sin(theta[7]))+
            cos(theta[3])*(cos(theta[5])*cos(theta[7])-sin(theta[5])*cos(theta[6])*sin(theta[7]))))*sin(theta[8]);


    R.at<double>(0,2) = (cos(theta[1])*(cos(theta[2])*
            (cos(theta[3])*(cos(theta[4])*cos(theta[5])*sin(theta[6])+sin(theta[4])*cos(theta[6]))-sin(theta[3])*sin(theta[5])*sin(theta[6]))+sin(theta[2])*
            (cos(theta[4])*cos(theta[6])-sin(theta[4])*cos(theta[5])*sin(theta[6])))-sin(theta[1])*
            (sin(theta[3])*(cos(theta[4])*cos(theta[5])*sin(theta[6])+sin(theta[4])*cos(theta[6]))+cos(theta[3])*sin(theta[5])*sin(theta[6])))*cos(theta[8])-(
            sin(theta[1])*(cos(theta[2])*(cos(theta[3])*(cos(theta[4])*cos(theta[5])*sin(theta[6])+sin(theta[4])*cos(theta[6]))-sin(theta[3])*sin(theta[5])*sin(theta[6]))
            +sin(theta[2])*(cos(theta[4])*cos(theta[6])-sin(theta[4])*cos(theta[5])*sin(theta[6])))+cos(theta[1])*
            (sin(theta[3])*(cos(theta[4])*cos(theta[5])*sin(theta[6])+sin(theta[4])*cos(theta[6]))+cos(theta[3])*sin(theta[5])*sin(theta[6])))*sin(theta[8]);

    R.at<double>(1,0) = (cos(theta[1])*(cos(theta[2])*(cos(theta[3])*
            (cos(theta[4])*(cos(theta[5])*cos(theta[6])*cos(theta[7])-sin(theta[5])*sin(theta[7]))-sin(theta[4])*sin(theta[6])*cos(theta[7]))-sin(theta[3])*
            (cos(theta[5])*sin(theta[7])+sin(theta[5])*cos(theta[6])*cos(theta[7])))+sin(theta[2])*
            (-sin(theta[4])*(cos(theta[5])*cos(theta[6])*cos(theta[7])-sin(theta[5])*sin(theta[7]))-cos(theta[4])*sin(theta[6])*cos(theta[7])))-sin(theta[1])*(
            sin(theta[3])*(cos(theta[4])*(cos(theta[5])*cos(theta[6])*cos(theta[7])-sin(theta[5])*sin(theta[7]))-sin(theta[4])*sin(theta[6])*cos(theta[7]))+cos(theta[3])*
            (cos(theta[5])*sin(theta[7])+sin(theta[5])*cos(theta[6])*cos(theta[7]))))*sin(theta[8])+(sin(theta[1])*(cos(theta[2])*(cos(theta[3])*
            (cos(theta[4])*(cos(theta[5])*cos(theta[6])*cos(theta[7])-sin(theta[5])*sin(theta[7]))-sin(theta[4])*sin(theta[6])*cos(theta[7]))-sin(theta[3])*
            (cos(theta[5])*sin(theta[7])+sin(theta[5])*cos(theta[6])*cos(theta[7])))+sin(theta[2])*
            (-sin(theta[4])*(cos(theta[5])*cos(theta[6])*cos(theta[7])-sin(theta[5])*sin(theta[7]))-cos(theta[4])*sin(theta[6])*cos(theta[7])))+cos(theta[1])*(
            sin(theta[3])*(cos(theta[4])*(cos(theta[5])*cos(theta[6])*cos(theta[7])-sin(theta[5])*sin(theta[7]))-sin(theta[4])*sin(theta[6])*cos(theta[7]))+cos(theta[3])*
            (cos(theta[5])*sin(theta[7])+sin(theta[5])*cos(theta[6])*cos(theta[7]))))*cos(theta[8]);


    R.at<double>(1,1) = (cos(theta[1])*(cos(theta[2])*(cos(theta[3])*
            (cos(theta[4])*(-cos(theta[5])*cos(theta[6])*sin(theta[7])-sin(theta[5])*cos(theta[7]))+sin(theta[4])*sin(theta[6])*sin(theta[7]))-sin(theta[3])*
            (cos(theta[5])*cos(theta[7])-sin(theta[5])*cos(theta[6])*sin(theta[7])))+sin(theta[2])*
            (cos(theta[4])*sin(theta[6])*sin(theta[7])-sin(theta[4])*(-cos(theta[5])*cos(theta[6])*sin(theta[7])-sin(theta[5])*cos(theta[7]))))-sin(theta[1])*(
            sin(theta[3])*(cos(theta[4])*(-cos(theta[5])*cos(theta[6])*sin(theta[7])-sin(theta[5])*cos(theta[7]))+sin(theta[4])*sin(theta[6])*sin(theta[7]))+
            cos(theta[3])*(cos(theta[5])*cos(theta[7])-sin(theta[5])*cos(theta[6])*sin(theta[7]))))*sin(theta[8])+(sin(theta[1])*(cos(theta[2])*(cos(theta[3])*
            (cos(theta[4])*(-cos(theta[5])*cos(theta[6])*sin(theta[7])-sin(theta[5])*cos(theta[7]))+sin(theta[4])*sin(theta[6])*sin(theta[7]))-sin(theta[3])*
            (cos(theta[5])*cos(theta[7])-sin(theta[5])*cos(theta[6])*sin(theta[7])))+sin(theta[2])*
            (cos(theta[4])*sin(theta[6])*sin(theta[7])-sin(theta[4])*(-cos(theta[5])*cos(theta[6])*sin(theta[7])-sin(theta[5])*cos(theta[7]))))+cos(theta[1])*(
            sin(theta[3])*(cos(theta[4])*(-cos(theta[5])*cos(theta[6])*sin(theta[7])-sin(theta[5])*cos(theta[7]))+sin(theta[4])*sin(theta[6])*sin(theta[7]))+
            cos(theta[3])*(cos(theta[5])*cos(theta[7])-sin(theta[5])*cos(theta[6])*sin(theta[7]))))*cos(theta[8]);



    R.at<double>(1,2) = (cos(theta[1])*(cos(theta[2])*
            (cos(theta[3])*(cos(theta[4])*cos(theta[5])*sin(theta[6])+sin(theta[4])*cos(theta[6]))-sin(theta[3])*sin(theta[5])*sin(theta[6]))+sin(theta[2])*
            (cos(theta[4])*cos(theta[6])-sin(theta[4])*cos(theta[5])*sin(theta[6])))-sin(theta[1])*
            (sin(theta[3])*(cos(theta[4])*cos(theta[5])*sin(theta[6])+sin(theta[4])*cos(theta[6]))+cos(theta[3])*sin(theta[5])*sin(theta[6])))*sin(theta[8])+(
            sin(theta[1])*(cos(theta[2])*(cos(theta[3])*(cos(theta[4])*cos(theta[5])*sin(theta[6])+sin(theta[4])*cos(theta[6]))-sin(theta[3])*sin(theta[5])*sin(theta[6]))
            +sin(theta[2])*(cos(theta[4])*cos(theta[6])-sin(theta[4])*cos(theta[5])*sin(theta[6])))+cos(theta[1])*
            (sin(theta[3])*(cos(theta[4])*cos(theta[5])*sin(theta[6])+sin(theta[4])*cos(theta[6]))+cos(theta[3])*sin(theta[5])*sin(theta[6])))*cos(theta[8]);

    R.at<double>(2,0) = cos(theta[2])*(-sin(theta[4])*(cos(theta[5])*cos(theta[6])*cos(theta[7])-sin(theta[5])*sin(theta[7]))-cos(theta[4])*sin(theta[6])*cos(theta[7]))-
                sin(theta[2])*(cos(theta[3])*(cos(theta[4])*(cos(theta[5])*cos(theta[6])*cos(theta[7])-sin(theta[5])*sin(theta[7]))-sin(theta[4])*sin(theta[6])*cos(theta[7]))
                -sin(theta[3])*(cos(theta[5])*sin(theta[7])+sin(theta[5])*cos(theta[6])*cos(theta[7])));

    R.at<double>(2,1) =  cos(theta[2])*(cos(theta[4])*sin(theta[6])*sin(theta[7])-sin(theta[4])*(-cos(theta[5])*cos(theta[6])*sin(theta[7])-sin(theta[5])*cos(theta[7])))-
                sin(theta[2])*(cos(theta[3])*
                (cos(theta[4])*(-cos(theta[5])*cos(theta[6])*sin(theta[7])-sin(theta[5])*cos(theta[7]))+sin(theta[4])*sin(theta[6])*sin(theta[7]))-sin(theta[3])*
                (cos(theta[5])*cos(theta[7])-sin(theta[5])*cos(theta[6])*sin(theta[7])));

    R.at<double>(2,2) = cos(theta[2])*(cos(theta[4])*cos(theta[6])-sin(theta[4])*cos(theta[5])*sin(theta[6]))-sin(theta[2])*
            (cos(theta[3])*(cos(theta[4])*cos(theta[5])*sin(theta[6])+sin(theta[4])*cos(theta[6]))-sin(theta[3])*sin(theta[5])*sin(theta[6]));
}

//==========================================================

// GWAM forward kinematics that returns the position and
// orientation of the end-effector
// Input: Joint angle vector: theta[7]
// Output: end-effector pose: xpose[6];
// Orientation is in terms of X-Y-Z fixed angles (gamma -->roll, beta --> pitch, alpha --> Yaw)
//---------------------------------------------
void mmanip_pose_fk(double Th[], double xp[], bool & VALID)
{

    double theta[9], x, y;
    theta[1] = Th[0];
    theta[2] = Th[1];
    theta[3] = Th[2];
    theta[4] = Th[3];
    theta[5] = Th[4];
    theta[6] = Th[5];
    theta[7] = Th[6];
    theta[8] = Th[7];
    x = Th[8];
    y = Th[9];


    xp[0] = x-sin(theta[8])*(sin(theta[1])*(cos(theta[2])*(cos(theta[3])*
            (sin(theta[4])*(cos(theta[6])*d7+d5)+cos(theta[4])*cos(theta[5])*sin(theta[6])*d7+cos(theta[4])*a4)-sin(theta[3])*sin(theta[5])*sin(theta[6])*d7+
            cos(theta[3])*a3)+sin(theta[2])*(cos(theta[4])*(cos(theta[6])*d7+d5)-sin(theta[4])*cos(theta[5])*sin(theta[6])*d7+d3-sin(theta[4])*a4))+cos(theta[1])*(
            sin(theta[3])*(sin(theta[4])*(cos(theta[6])*d7+d5)+cos(theta[4])*cos(theta[5])*sin(theta[6])*d7+cos(theta[4])*a4)+cos(theta[3])*sin(theta[5])*sin(theta[6])
            *d7+sin(theta[3])*a3))+cos(theta[8])*(cos(theta[1])*(cos(theta[2])*(cos(theta[3])*
            (sin(theta[4])*(cos(theta[6])*d7+d5)+cos(theta[4])*cos(theta[5])*sin(theta[6])*d7+cos(theta[4])*a4)-sin(theta[3])*sin(theta[5])*sin(theta[6])*d7+
            cos(theta[3])*a3)+sin(theta[2])*(cos(theta[4])*(cos(theta[6])*d7+d5)-sin(theta[4])*cos(theta[5])*sin(theta[6])*d7+d3-sin(theta[4])*a4))-sin(theta[1])*(
            sin(theta[3])*(sin(theta[4])*(cos(theta[6])*d7+d5)+cos(theta[4])*cos(theta[5])*sin(theta[6])*d7+cos(theta[4])*a4)+cos(theta[3])*sin(theta[5])*sin(theta[6])
            *d7+sin(theta[3])*a3));

    xp[1] = y+cos(theta[8])*(sin(theta[1])*(cos(theta[2])*(cos(theta[3])*
            (sin(theta[4])*(cos(theta[6])*d7+d5)+cos(theta[4])*cos(theta[5])*sin(theta[6])*d7+cos(theta[4])*a4)-sin(theta[3])*sin(theta[5])*sin(theta[6])*d7+
            cos(theta[3])*a3)+sin(theta[2])*(cos(theta[4])*(cos(theta[6])*d7+d5)-sin(theta[4])*cos(theta[5])*sin(theta[6])*d7+d3-sin(theta[4])*a4))+cos(theta[1])*(
            sin(theta[3])*(sin(theta[4])*(cos(theta[6])*d7+d5)+cos(theta[4])*cos(theta[5])*sin(theta[6])*d7+cos(theta[4])*a4)+cos(theta[3])*sin(theta[5])*sin(theta[6])
            *d7+sin(theta[3])*a3))+sin(theta[8])*(cos(theta[1])*(cos(theta[2])*(cos(theta[3])*
            (sin(theta[4])*(cos(theta[6])*d7+d5)+cos(theta[4])*cos(theta[5])*sin(theta[6])*d7+cos(theta[4])*a4)-sin(theta[3])*sin(theta[5])*sin(theta[6])*d7+
            cos(theta[3])*a3)+sin(theta[2])*(cos(theta[4])*(cos(theta[6])*d7+d5)-sin(theta[4])*cos(theta[5])*sin(theta[6])*d7+d3-sin(theta[4])*a4))-sin(theta[1])*(
            sin(theta[3])*(sin(theta[4])*(cos(theta[6])*d7+d5)+cos(theta[4])*cos(theta[5])*sin(theta[6])*d7+cos(theta[4])*a4)+cos(theta[3])*sin(theta[5])*sin(theta[6])
            *d7+sin(theta[3])*a3));


    xp[2] = db-sin(theta[2])*(cos(theta[3])*(sin(theta[4])*(cos(theta[6])*d7+d5)+cos(theta[4])*cos(theta[5])*sin(theta[6])*d7+cos(theta[4])*a4)-
                sin(theta[3])*sin(theta[5])*sin(theta[6])*d7+cos(theta[3])*a3)+cos(theta[2])*
                (cos(theta[4])*(cos(theta[6])*d7+d5)-sin(theta[4])*cos(theta[5])*sin(theta[6])*d7+d3-sin(theta[4])*a4);



    Mat R(3,3, CV_64F);
    rotation_matrix(Th, R); // Pass the original Th[] variable.

    // Beta - Pitch - Rotation about Y-axis. consider the positive value of the sqrt
    xp[3] = atan2(-R.at<double>(2,0), fabs(sqrt(pow(R.at<double>(0,0), 2) + pow(R.at<double>(1,0), 2.0) )) );

    if(xp[3] > -M_PI/2.0 && xp[3] < M_PI/2.0)
    {
        // Alpha - Roll- Rotation about z-axis
        xp[4] = atan2(R.at<double>(1,0)/cos(xp[3]), R.at<double>(0,0)/cos(xp[3]));

        // Gamma - Yaw - Rotation about X-axis
        xp[5] = atan2(R.at<double>(2,1)/cos(xp[3]), R.at<double>(2,2)/cos(xp[3]));

        VALID = true;
    }
    else if (xp[3] == M_PI/2.0)
    {
        xp[4] = 0.0;  //alpha
        xp[5] = atan2(R.at<double>(0,1), R.at<double>(1,1)); // gamma

        VALID = true;
    }
    else if(xp[3] == -M_PI/2.0)
    {
        xp[4] = 0.0;
        xp[5] = -atan2(R.at<double>(0,1), R.at<double>(1,1));

        VALID = true;
    }
    else
    {
        VALID = false;
    }
}
//=========================================
// Generates input-output data for the manipulator
void generate_data(double Uc[], double Th[])
{
    do
    {
        for(int i = 0; i < NL; i++)
            Th[i] = theta_min[i] + (theta_max[i] - theta_min[i]) *
                    (rand()/(double)RAND_MAX);

        //So that Ut is in the workspace of robot
        mmanip_7dof_FK(Th,Uc);

        if( (Uc[0] >= c_min[0]) && (Uc[0] < c_max[0]) &&
                (Uc[1] >= c_min[1]) && (Uc[1] < c_max[1]) &&
                (Uc[2] >= c_min[2]) && (Uc[2] < c_max[2]) )
            break;
    }while(1);
}


//============================================================

// Generates input-output data for the manipulator
// Input: Joint angles
// Output: End-effector pose in 6D
void generate_pose_data(double Uc[], double Th[])
{
    bool repeat_flag = true;
    do
    {
        bool VALID = true;
        for(int i = 0; i < NL; i++)
            Th[i] = theta_min[i] + (theta_max[i] - theta_min[i]) *
                    (rand()/(double)RAND_MAX);

        // find the end-effector pose for a given theta vector
        // forward kinematics
        mmanip_pose_fk(Th, Uc, VALID);

        if(VALID == true)
        {
            if( (Uc[0] >= c_min[0]) && (Uc[0] < c_max[0]) &&
                    (Uc[1] >= c_min[1]) && (Uc[1] < c_max[1]) &&
                    (Uc[2] >= c_min[2]) && (Uc[2] < c_max[2]) )
                break;
        }
    }while(1);

}

//=================================================================
// Jp : dX /d theta : 3x7
void position_jacobian(double Th[], cv::Mat &Jp)
{


    double theta[9], x, y;
    theta[1] = Th[0];
    theta[2] = Th[1];
    theta[3] = Th[2];
    theta[4] = Th[3];
    theta[5] = Th[4];
    theta[6] = Th[5];
    theta[7] = Th[6];
    theta[8] = Th[7];
    x = Th[8];
    y = Th[9];


   Jp.at<double>(0,0) = cos(theta[8])*(-sin(theta[1])*(cos(theta[2])*(cos(theta[3])*
           (sin(theta[4])*(cos(theta[6])*d7+d5)+cos(theta[4])*cos(theta[5])*sin(theta[6])*d7+cos(theta[4])*a4)-sin(theta[3])*sin(theta[5])*sin(theta[6])*d7+
           cos(theta[3])*a3)+sin(theta[2])*(cos(theta[4])*(cos(theta[6])*d7+d5)-sin(theta[4])*cos(theta[5])*sin(theta[6])*d7+d3-sin(theta[4])*a4))-cos(theta[1])*(
           sin(theta[3])*(sin(theta[4])*(cos(theta[6])*d7+d5)+cos(theta[4])*cos(theta[5])*sin(theta[6])*d7+cos(theta[4])*a4)+cos(theta[3])*sin(theta[5])*sin(theta[6])
           *d7+sin(theta[3])*a3))-sin(theta[8])*(cos(theta[1])*(cos(theta[2])*(cos(theta[3])*
           (sin(theta[4])*(cos(theta[6])*d7+d5)+cos(theta[4])*cos(theta[5])*sin(theta[6])*d7+cos(theta[4])*a4)-sin(theta[3])*sin(theta[5])*sin(theta[6])*d7+
           cos(theta[3])*a3)+sin(theta[2])*(cos(theta[4])*(cos(theta[6])*d7+d5)-sin(theta[4])*cos(theta[5])*sin(theta[6])*d7+d3-sin(theta[4])*a4))-sin(theta[1])*(
           sin(theta[3])*(sin(theta[4])*(cos(theta[6])*d7+d5)+cos(theta[4])*cos(theta[5])*sin(theta[6])*d7+cos(theta[4])*a4)+cos(theta[3])*sin(theta[5])*sin(theta[6])
           *d7+sin(theta[3])*a3));

   Jp.at<double>(0,1) = cos(theta[1])*cos(theta[8])*(cos(theta[2])*
           (cos(theta[4])*(cos(theta[6])*d7+d5)-sin(theta[4])*cos(theta[5])*sin(theta[6])*d7+d3-sin(theta[4])*a4)-sin(theta[2])*(cos(theta[3])*
           (sin(theta[4])*(cos(theta[6])*d7+d5)+cos(theta[4])*cos(theta[5])*sin(theta[6])*d7+cos(theta[4])*a4)-sin(theta[3])*sin(theta[5])*sin(theta[6])*d7+
           cos(theta[3])*a3))-sin(theta[1])*sin(theta[8])*(cos(theta[2])*
           (cos(theta[4])*(cos(theta[6])*d7+d5)-sin(theta[4])*cos(theta[5])*sin(theta[6])*d7+d3-sin(theta[4])*a4)-sin(theta[2])*(cos(theta[3])*
           (sin(theta[4])*(cos(theta[6])*d7+d5)+cos(theta[4])*cos(theta[5])*sin(theta[6])*d7+cos(theta[4])*a4)-sin(theta[3])*sin(theta[5])*sin(theta[6])*d7+
           cos(theta[3])*a3));

   Jp.at<double>(0,2) = cos(theta[8])*(cos(theta[1])*cos(theta[2])*(-sin(theta[3])*
           (sin(theta[4])*(cos(theta[6])*d7+d5)+cos(theta[4])*cos(theta[5])*sin(theta[6])*d7+cos(theta[4])*a4)-cos(theta[3])*sin(theta[5])*sin(theta[6])*d7-
           sin(theta[3])*a3)-sin(theta[1])*(cos(theta[3])*(sin(theta[4])*(cos(theta[6])*d7+d5)+cos(theta[4])*cos(theta[5])*sin(theta[6])*d7+cos(theta[4])*a4)-
           sin(theta[3])*sin(theta[5])*sin(theta[6])*d7+cos(theta[3])*a3))-sin(theta[8])*(sin(theta[1])*cos(theta[2])*(-sin(theta[3])*
           (sin(theta[4])*(cos(theta[6])*d7+d5)+cos(theta[4])*cos(theta[5])*sin(theta[6])*d7+cos(theta[4])*a4)-cos(theta[3])*sin(theta[5])*sin(theta[6])*d7-
           sin(theta[3])*a3)+cos(theta[1])*(cos(theta[3])*(sin(theta[4])*(cos(theta[6])*d7+d5)+cos(theta[4])*cos(theta[5])*sin(theta[6])*d7+cos(theta[4])*a4)-
           sin(theta[3])*sin(theta[5])*sin(theta[6])*d7+cos(theta[3])*a3));

   Jp.at<double>(0,3) = cos(theta[8])*(cos(theta[1])*(sin(theta[2])*(-sin(theta[4])*(cos(theta[6])*d7+d5)-cos(theta[4])*cos(theta[5])*sin(theta[6])*d7-cos(theta[4])*a4)
           +cos(theta[2])*cos(theta[3])*(cos(theta[4])*(cos(theta[6])*d7+d5)-sin(theta[4])*cos(theta[5])*sin(theta[6])*d7-sin(theta[4])*a4))-sin(theta[1])*
           sin(theta[3])*(cos(theta[4])*(cos(theta[6])*d7+d5)-sin(theta[4])*cos(theta[5])*sin(theta[6])*d7-sin(theta[4])*a4))-sin(theta[8])*(sin(theta[1])*(
           sin(theta[2])*(-sin(theta[4])*(cos(theta[6])*d7+d5)-cos(theta[4])*cos(theta[5])*sin(theta[6])*d7-cos(theta[4])*a4)+cos(theta[2])*cos(theta[3])*
           (cos(theta[4])*(cos(theta[6])*d7+d5)-sin(theta[4])*cos(theta[5])*sin(theta[6])*d7-sin(theta[4])*a4))+cos(theta[1])*sin(theta[3])*
           (cos(theta[4])*(cos(theta[6])*d7+d5)-sin(theta[4])*cos(theta[5])*sin(theta[6])*d7-sin(theta[4])*a4));

   Jp.at<double>(0,4) = cos(theta[8])*(cos(theta[1])*(cos(theta[2])*
           (-cos(theta[3])*cos(theta[4])*sin(theta[5])*sin(theta[6])*d7-sin(theta[3])*cos(theta[5])*sin(theta[6])*d7)+sin(theta[2])*sin(theta[4])*sin(theta[5])*
           sin(theta[6])*d7)-sin(theta[1])*(cos(theta[3])*cos(theta[5])*sin(theta[6])*d7-sin(theta[3])*cos(theta[4])*sin(theta[5])*sin(theta[6])*d7))-sin(theta[8])*(
           sin(theta[1])*(cos(theta[2])*(-cos(theta[3])*cos(theta[4])*sin(theta[5])*sin(theta[6])*d7-sin(theta[3])*cos(theta[5])*sin(theta[6])*d7)+sin(theta[2])*
           sin(theta[4])*sin(theta[5])*sin(theta[6])*d7)+cos(theta[1])*
           (cos(theta[3])*cos(theta[5])*sin(theta[6])*d7-sin(theta[3])*cos(theta[4])*sin(theta[5])*sin(theta[6])*d7));

   Jp.at<double>(0,5) = cos(theta[8])*(cos(theta[1])*(cos(theta[2])*
           (cos(theta[3])*(cos(theta[4])*cos(theta[5])*cos(theta[6])*d7-sin(theta[4])*sin(theta[6])*d7)-sin(theta[3])*sin(theta[5])*cos(theta[6])*d7)+sin(theta[2])*
           (-cos(theta[4])*sin(theta[6])*d7-sin(theta[4])*cos(theta[5])*cos(theta[6])*d7))-sin(theta[1])*
           (sin(theta[3])*(cos(theta[4])*cos(theta[5])*cos(theta[6])*d7-sin(theta[4])*sin(theta[6])*d7)+cos(theta[3])*sin(theta[5])*cos(theta[6])*d7))-sin(theta[8])*(
           sin(theta[1])*(cos(theta[2])*
           (cos(theta[3])*(cos(theta[4])*cos(theta[5])*cos(theta[6])*d7-sin(theta[4])*sin(theta[6])*d7)-sin(theta[3])*sin(theta[5])*cos(theta[6])*d7)+sin(theta[2])*
           (-cos(theta[4])*sin(theta[6])*d7-sin(theta[4])*cos(theta[5])*cos(theta[6])*d7))+cos(theta[1])*
           (sin(theta[3])*(cos(theta[4])*cos(theta[5])*cos(theta[6])*d7-sin(theta[4])*sin(theta[6])*d7)+cos(theta[3])*sin(theta[5])*cos(theta[6])*d7));

   Jp.at<double>(0,6) = 0;

   Jp.at<double>(0,7) = -cos(theta[8])*(sin(theta[1])*(cos(theta[2])*(cos(theta[3])*
           (sin(theta[4])*(cos(theta[6])*d7+d5)+cos(theta[4])*cos(theta[5])*sin(theta[6])*d7+cos(theta[4])*a4)-sin(theta[3])*sin(theta[5])*sin(theta[6])*d7+
           cos(theta[3])*a3)+sin(theta[2])*(cos(theta[4])*(cos(theta[6])*d7+d5)-sin(theta[4])*cos(theta[5])*sin(theta[6])*d7+d3-sin(theta[4])*a4))+cos(theta[1])*(
           sin(theta[3])*(sin(theta[4])*(cos(theta[6])*d7+d5)+cos(theta[4])*cos(theta[5])*sin(theta[6])*d7+cos(theta[4])*a4)+cos(theta[3])*sin(theta[5])*sin(theta[6])
           *d7+sin(theta[3])*a3))-sin(theta[8])*(cos(theta[1])*(cos(theta[2])*(cos(theta[3])*
           (sin(theta[4])*(cos(theta[6])*d7+d5)+cos(theta[4])*cos(theta[5])*sin(theta[6])*d7+cos(theta[4])*a4)-sin(theta[3])*sin(theta[5])*sin(theta[6])*d7+
           cos(theta[3])*a3)+sin(theta[2])*(cos(theta[4])*(cos(theta[6])*d7+d5)-sin(theta[4])*cos(theta[5])*sin(theta[6])*d7+d3-sin(theta[4])*a4))-sin(theta[1])*(
           sin(theta[3])*(sin(theta[4])*(cos(theta[6])*d7+d5)+cos(theta[4])*cos(theta[5])*sin(theta[6])*d7+cos(theta[4])*a4)+cos(theta[3])*sin(theta[5])*sin(theta[6])
           *d7+sin(theta[3])*a3));

   Jp.at<double>(0,8) = 1;
   Jp.at<double>(0,9) = 0;

   //-----------------

   Jp.at<double>(1,0) = sin(theta[8])*(-sin(theta[1])*(cos(theta[2])*(cos(theta[3])*
           (sin(theta[4])*(cos(theta[6])*d7+d5)+cos(theta[4])*cos(theta[5])*sin(theta[6])*d7+cos(theta[4])*a4)-sin(theta[3])*sin(theta[5])*sin(theta[6])*d7+
           cos(theta[3])*a3)+sin(theta[2])*(cos(theta[4])*(cos(theta[6])*d7+d5)-sin(theta[4])*cos(theta[5])*sin(theta[6])*d7+d3-sin(theta[4])*a4))-cos(theta[1])*(
           sin(theta[3])*(sin(theta[4])*(cos(theta[6])*d7+d5)+cos(theta[4])*cos(theta[5])*sin(theta[6])*d7+cos(theta[4])*a4)+cos(theta[3])*sin(theta[5])*sin(theta[6])
           *d7+sin(theta[3])*a3))+cos(theta[8])*(cos(theta[1])*(cos(theta[2])*(cos(theta[3])*
           (sin(theta[4])*(cos(theta[6])*d7+d5)+cos(theta[4])*cos(theta[5])*sin(theta[6])*d7+cos(theta[4])*a4)-sin(theta[3])*sin(theta[5])*sin(theta[6])*d7+
           cos(theta[3])*a3)+sin(theta[2])*(cos(theta[4])*(cos(theta[6])*d7+d5)-sin(theta[4])*cos(theta[5])*sin(theta[6])*d7+d3-sin(theta[4])*a4))-sin(theta[1])*(
           sin(theta[3])*(sin(theta[4])*(cos(theta[6])*d7+d5)+cos(theta[4])*cos(theta[5])*sin(theta[6])*d7+cos(theta[4])*a4)+cos(theta[3])*sin(theta[5])*sin(theta[6])
           *d7+sin(theta[3])*a3));

   Jp.at<double>(1,1) = cos(theta[1])*sin(theta[8])*(cos(theta[2])*
           (cos(theta[4])*(cos(theta[6])*d7+d5)-sin(theta[4])*cos(theta[5])*sin(theta[6])*d7+d3-sin(theta[4])*a4)-sin(theta[2])*(cos(theta[3])*
           (sin(theta[4])*(cos(theta[6])*d7+d5)+cos(theta[4])*cos(theta[5])*sin(theta[6])*d7+cos(theta[4])*a4)-sin(theta[3])*sin(theta[5])*sin(theta[6])*d7+
           cos(theta[3])*a3))+sin(theta[1])*cos(theta[8])*(cos(theta[2])*
           (cos(theta[4])*(cos(theta[6])*d7+d5)-sin(theta[4])*cos(theta[5])*sin(theta[6])*d7+d3-sin(theta[4])*a4)-sin(theta[2])*(cos(theta[3])*
           (sin(theta[4])*(cos(theta[6])*d7+d5)+cos(theta[4])*cos(theta[5])*sin(theta[6])*d7+cos(theta[4])*a4)-sin(theta[3])*sin(theta[5])*sin(theta[6])*d7+
           cos(theta[3])*a3));



   Jp.at<double>(1,2) = cos(theta[8])*(sin(theta[1])*cos(theta[2])*(-sin(theta[3])*
           (sin(theta[4])*(cos(theta[6])*d7+d5)+cos(theta[4])*cos(theta[5])*sin(theta[6])*d7+cos(theta[4])*a4)-cos(theta[3])*sin(theta[5])*sin(theta[6])*d7-
           sin(theta[3])*a3)+cos(theta[1])*(cos(theta[3])*(sin(theta[4])*(cos(theta[6])*d7+d5)+cos(theta[4])*cos(theta[5])*sin(theta[6])*d7+cos(theta[4])*a4)-
           sin(theta[3])*sin(theta[5])*sin(theta[6])*d7+cos(theta[3])*a3))+sin(theta[8])*(cos(theta[1])*cos(theta[2])*(-sin(theta[3])*
           (sin(theta[4])*(cos(theta[6])*d7+d5)+cos(theta[4])*cos(theta[5])*sin(theta[6])*d7+cos(theta[4])*a4)-cos(theta[3])*sin(theta[5])*sin(theta[6])*d7-
           sin(theta[3])*a3)-sin(theta[1])*(cos(theta[3])*(sin(theta[4])*(cos(theta[6])*d7+d5)+cos(theta[4])*cos(theta[5])*sin(theta[6])*d7+cos(theta[4])*a4)-
           sin(theta[3])*sin(theta[5])*sin(theta[6])*d7+cos(theta[3])*a3));

   Jp.at<double>(1,3) = cos(theta[8])*(sin(theta[1])*(sin(theta[2])*(-sin(theta[4])*(cos(theta[6])*d7+d5)-cos(theta[4])*cos(theta[5])*sin(theta[6])*d7-cos(theta[4])*a4)
           +cos(theta[2])*cos(theta[3])*(cos(theta[4])*(cos(theta[6])*d7+d5)-sin(theta[4])*cos(theta[5])*sin(theta[6])*d7-sin(theta[4])*a4))+cos(theta[1])*
           sin(theta[3])*(cos(theta[4])*(cos(theta[6])*d7+d5)-sin(theta[4])*cos(theta[5])*sin(theta[6])*d7-sin(theta[4])*a4))+sin(theta[8])*(cos(theta[1])*(
           sin(theta[2])*(-sin(theta[4])*(cos(theta[6])*d7+d5)-cos(theta[4])*cos(theta[5])*sin(theta[6])*d7-cos(theta[4])*a4)+cos(theta[2])*cos(theta[3])*
           (cos(theta[4])*(cos(theta[6])*d7+d5)-sin(theta[4])*cos(theta[5])*sin(theta[6])*d7-sin(theta[4])*a4))-sin(theta[1])*sin(theta[3])*
           (cos(theta[4])*(cos(theta[6])*d7+d5)-sin(theta[4])*cos(theta[5])*sin(theta[6])*d7-sin(theta[4])*a4));

   Jp.at<double>(1,4) = cos(theta[8])*(sin(theta[1])*(cos(theta[2])*
           (-cos(theta[3])*cos(theta[4])*sin(theta[5])*sin(theta[6])*d7-sin(theta[3])*cos(theta[5])*sin(theta[6])*d7)+sin(theta[2])*sin(theta[4])*sin(theta[5])*
           sin(theta[6])*d7)+cos(theta[1])*(cos(theta[3])*cos(theta[5])*sin(theta[6])*d7-sin(theta[3])*cos(theta[4])*sin(theta[5])*sin(theta[6])*d7))+sin(theta[8])*(
           cos(theta[1])*(cos(theta[2])*(-cos(theta[3])*cos(theta[4])*sin(theta[5])*sin(theta[6])*d7-sin(theta[3])*cos(theta[5])*sin(theta[6])*d7)+sin(theta[2])*
           sin(theta[4])*sin(theta[5])*sin(theta[6])*d7)-sin(theta[1])*
           (cos(theta[3])*cos(theta[5])*sin(theta[6])*d7-sin(theta[3])*cos(theta[4])*sin(theta[5])*sin(theta[6])*d7));


   Jp.at<double>(1,5) = cos(theta[8])*(sin(theta[1])*(cos(theta[2])*
           (cos(theta[3])*(cos(theta[4])*cos(theta[5])*cos(theta[6])*d7-sin(theta[4])*sin(theta[6])*d7)-sin(theta[3])*sin(theta[5])*cos(theta[6])*d7)+sin(theta[2])*
           (-cos(theta[4])*sin(theta[6])*d7-sin(theta[4])*cos(theta[5])*cos(theta[6])*d7))+cos(theta[1])*
           (sin(theta[3])*(cos(theta[4])*cos(theta[5])*cos(theta[6])*d7-sin(theta[4])*sin(theta[6])*d7)+cos(theta[3])*sin(theta[5])*cos(theta[6])*d7))+sin(theta[8])*(
           cos(theta[1])*(cos(theta[2])*
           (cos(theta[3])*(cos(theta[4])*cos(theta[5])*cos(theta[6])*d7-sin(theta[4])*sin(theta[6])*d7)-sin(theta[3])*sin(theta[5])*cos(theta[6])*d7)+sin(theta[2])*
           (-cos(theta[4])*sin(theta[6])*d7-sin(theta[4])*cos(theta[5])*cos(theta[6])*d7))-sin(theta[1])*
           (sin(theta[3])*(cos(theta[4])*cos(theta[5])*cos(theta[6])*d7-sin(theta[4])*sin(theta[6])*d7)+cos(theta[3])*sin(theta[5])*cos(theta[6])*d7));


   Jp.at<double>(1,6) = 0;

   Jp.at<double>(1,7) = cos(theta[8])*(cos(theta[1])*(cos(theta[2])*(cos(theta[3])*
           (sin(theta[4])*(cos(theta[6])*d7+d5)+cos(theta[4])*cos(theta[5])*sin(theta[6])*d7+cos(theta[4])*a4)-sin(theta[3])*sin(theta[5])*sin(theta[6])*d7+
           cos(theta[3])*a3)+sin(theta[2])*(cos(theta[4])*(cos(theta[6])*d7+d5)-sin(theta[4])*cos(theta[5])*sin(theta[6])*d7+d3-sin(theta[4])*a4))-sin(theta[1])*(
           sin(theta[3])*(sin(theta[4])*(cos(theta[6])*d7+d5)+cos(theta[4])*cos(theta[5])*sin(theta[6])*d7+cos(theta[4])*a4)+cos(theta[3])*sin(theta[5])*sin(theta[6])
           *d7+sin(theta[3])*a3))-sin(theta[8])*(sin(theta[1])*(cos(theta[2])*(cos(theta[3])*
           (sin(theta[4])*(cos(theta[6])*d7+d5)+cos(theta[4])*cos(theta[5])*sin(theta[6])*d7+cos(theta[4])*a4)-sin(theta[3])*sin(theta[5])*sin(theta[6])*d7+
           cos(theta[3])*a3)+sin(theta[2])*(cos(theta[4])*(cos(theta[6])*d7+d5)-sin(theta[4])*cos(theta[5])*sin(theta[6])*d7+d3-sin(theta[4])*a4))+cos(theta[1])*(
           sin(theta[3])*(sin(theta[4])*(cos(theta[6])*d7+d5)+cos(theta[4])*cos(theta[5])*sin(theta[6])*d7+cos(theta[4])*a4)+cos(theta[3])*sin(theta[5])*sin(theta[6])
           *d7+sin(theta[3])*a3));

   Jp.at<double>(1,8) = 0;

   Jp.at<double>(1,9) = 1;

   //-----------------------------


   Jp.at<double>(2,0) = 0.0;

   Jp.at<double>(2,1) = -cos(theta[2])*(cos(theta[3])*(sin(theta[4])*(cos(theta[6])*d7+d5)+cos(theta[4])*cos(theta[5])*sin(theta[6])*d7+cos(theta[4])*a4)-sin(theta[3])*
           sin(theta[5])*sin(theta[6])*d7+cos(theta[3])*a3)-sin(theta[2])*
           (cos(theta[4])*(cos(theta[6])*d7+d5)-sin(theta[4])*cos(theta[5])*sin(theta[6])*d7+d3-sin(theta[4])*a4);

   Jp.at<double>(2,2) = -sin(theta[2])*(-sin(theta[3])*(sin(theta[4])*(cos(theta[6])*d7+d5)+cos(theta[4])*cos(theta[5])*sin(theta[6])*d7+cos(theta[4])*a4)-
               cos(theta[3])*sin(theta[5])*sin(theta[6])*d7-sin(theta[3])*a3);


   Jp.at<double>(2,3) = cos(theta[2])*(-sin(theta[4])*(cos(theta[6])*d7+d5)-cos(theta[4])*cos(theta[5])*sin(theta[6])*d7-cos(theta[4])*a4)-sin(theta[2])*cos(theta[3])*
           (cos(theta[4])*(cos(theta[6])*d7+d5)-sin(theta[4])*cos(theta[5])*sin(theta[6])*d7-sin(theta[4])*a4);

   Jp.at<double>(2,4) = cos(theta[2])*sin(theta[4])*sin(theta[5])*sin(theta[6])*d7-sin(theta[2])*
           (-cos(theta[3])*cos(theta[4])*sin(theta[5])*sin(theta[6])*d7-sin(theta[3])*cos(theta[5])*sin(theta[6])*d7);

   Jp.at<double>(2,5) = cos(theta[2])*(-cos(theta[4])*sin(theta[6])*d7-sin(theta[4])*cos(theta[5])*cos(theta[6])*d7)-sin(theta[2])*
           (cos(theta[3])*(cos(theta[4])*cos(theta[5])*cos(theta[6])*d7-sin(theta[4])*sin(theta[6])*d7)-sin(theta[3])*sin(theta[5])*cos(theta[6])*d7);

   Jp.at<double>(2,6) = 0.0;

   Jp.at<double>(2,7) = 0.0;

   Jp.at<double>(2,8) = 0.0;
   Jp.at<double>(2,9) = 0.0;

}

//------------------------------------------------

// This is obtained by using Euler Angle representation

void orientation_jacobian(double Th[], cv::Mat &Jw)
{

    double theta[9], x, y;
    theta[1] = Th[0];
    theta[2] = Th[1];
    theta[3] = Th[2];
    theta[4] = Th[3];
    theta[5] = Th[4];
    theta[6] = Th[5];
    theta[7] = Th[6];
    theta[8] = Th[7];
    x = Th[8];
    y = Th[9];


    // Rotation Matrix
    Mat R = Mat(3,3, CV_64F);
    rotation_matrix(Th, R);  //pass the original Th[] vector


    double r21 = R.at<double>(1,0);
    double r11 = R.at<double>(0,0);
    double r31 = R.at<double>(2,0);
    double r32 = R.at<double>(2,1);
    double r33 = R.at<double>(2,2);
    double r22 = R.at<double>(1,1);
    double r12 = R.at<double>(0,1);

    double beta = atan2(-r31, abs(sqrt(r11*r11 + r21*r21)));



    if(beta > -M_PI/2.0 && beta < M_PI/2.0) // Case 1
    {


        Jw.at<double>(0,0) = -((((((sin(theta[1])*sin(theta[2])*sin(theta[4])+(-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*
                sin(theta[5])+(cos(theta[1])*cos(theta[3])-sin(theta[1])*cos(theta[2])*sin(theta[3]))*cos(theta[5]))*sin(theta[7])+(
                    ((-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[4])-sin(theta[1])*sin(theta[2])*cos(theta[4]))*sin(theta[6])+(
                    (cos(theta[1])*cos(theta[3])-sin(theta[1])*cos(theta[2])*sin(theta[3]))*sin(theta[5])+
                ((cos(theta[1])*sin(theta[3])+sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4])-sin(theta[1])*sin(theta[2])*sin(theta[4]))*cos(theta[5]))*cos(theta[6])
                )*cos(theta[7]))*sin(theta[8])+((
                                                    ((cos(theta[1])*cos(theta[2])*cos(theta[3])-sin(theta[1])*sin(theta[3]))*cos(theta[4])-cos(theta[1])*sin(theta[2])*sin(theta[4]))*sin(theta[5])+
                (cos(theta[1])*cos(theta[2])*sin(theta[3])+sin(theta[1])*cos(theta[3]))*cos(theta[5]))*sin(theta[7])+(
                    ((cos(theta[1])*cos(theta[2])*cos(theta[3])-sin(theta[1])*sin(theta[3]))*sin(theta[4])+cos(theta[1])*sin(theta[2])*cos(theta[4]))*sin(theta[6])+(
                    (cos(theta[1])*cos(theta[2])*sin(theta[3])+sin(theta[1])*cos(theta[3]))*sin(theta[5])+
                (cos(theta[1])*sin(theta[2])*sin(theta[4])+(sin(theta[1])*sin(theta[3])-cos(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*cos(theta[5]))*cos(theta[6])
                )*cos(theta[7]))*cos(theta[8]))*r21+(((
                                                          (cos(theta[1])*sin(theta[2])*sin(theta[4])+(sin(theta[1])*sin(theta[3])-cos(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*sin(theta[5])+
                (-cos(theta[1])*cos(theta[2])*sin(theta[3])-sin(theta[1])*cos(theta[3]))*cos(theta[5]))*sin(theta[7])+(
                    ((sin(theta[1])*sin(theta[3])-cos(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[4])-cos(theta[1])*sin(theta[2])*cos(theta[4]))*sin(theta[6])+(
                    (-cos(theta[1])*cos(theta[2])*sin(theta[3])-sin(theta[1])*cos(theta[3]))*sin(theta[5])+
                ((cos(theta[1])*cos(theta[2])*cos(theta[3])-sin(theta[1])*sin(theta[3]))*cos(theta[4])-cos(theta[1])*sin(theta[2])*sin(theta[4]))*cos(theta[5]))*cos(theta[6])
                )*cos(theta[7]))*sin(theta[8])+((
                                                    (sin(theta[1])*sin(theta[2])*sin(theta[4])+(-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*sin(theta[5])+
                (cos(theta[1])*cos(theta[3])-sin(theta[1])*cos(theta[2])*sin(theta[3]))*cos(theta[5]))*sin(theta[7])+(
                    ((-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[4])-sin(theta[1])*sin(theta[2])*cos(theta[4]))*sin(theta[6])+(
                    (cos(theta[1])*cos(theta[3])-sin(theta[1])*cos(theta[2])*sin(theta[3]))*sin(theta[5])+
                ((cos(theta[1])*sin(theta[3])+sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4])-sin(theta[1])*sin(theta[2])*sin(theta[4]))*cos(theta[5]))*cos(theta[6])
                )*cos(theta[7]))*cos(theta[8]))*r11)*r31)/(sqrt(r21*r21+r11*r11)*(r31*r31+r21*r21+r11*r11));


        Jw.at<double>(0,1) = ((((((cos(theta[1])*cos(theta[2])*sin(theta[4])+cos(theta[1])*sin(theta[2])*cos(theta[3])*cos(theta[4]))*sin(theta[5])+cos(theta[1])*sin(theta[2])*
                sin(theta[3])*cos(theta[5]))*sin(theta[7])+((cos(theta[1])*sin(theta[2])*cos(theta[3])*sin(theta[4])-cos(theta[1])*cos(theta[2])*cos(theta[4]))*sin(theta[6])
                +(cos(theta[1])*sin(theta[2])*sin(theta[3])*sin(theta[5])+
                (-cos(theta[1])*cos(theta[2])*sin(theta[4])-cos(theta[1])*sin(theta[2])*cos(theta[3])*cos(theta[4]))*cos(theta[5]))*cos(theta[6]))*cos(theta[7]))*
                sin(theta[8])+(((sin(theta[1])*cos(theta[2])*sin(theta[4])+sin(theta[1])*sin(theta[2])*cos(theta[3])*cos(theta[4]))*sin(theta[5])+sin(theta[1])*
                sin(theta[2])*sin(theta[3])*cos(theta[5]))*sin(theta[7])+((sin(theta[1])*sin(theta[2])*cos(theta[3])*sin(theta[4])-sin(theta[1])*cos(theta[2])*cos(theta[4]))*
                sin(theta[6])+(sin(theta[1])*sin(theta[2])*sin(theta[3])*sin(theta[5])+
                (-sin(theta[1])*cos(theta[2])*sin(theta[4])-sin(theta[1])*sin(theta[2])*cos(theta[3])*cos(theta[4]))*cos(theta[5]))*cos(theta[6]))*cos(theta[7]))*
                cos(theta[8]))*r21+((((-sin(theta[1])*cos(theta[2])*sin(theta[4])-sin(theta[1])*sin(theta[2])*cos(theta[3])*cos(theta[4]))*sin(theta[5])-sin(theta[1])*
                sin(theta[2])*sin(theta[3])*cos(theta[5]))*sin(theta[7])+((sin(theta[1])*cos(theta[2])*cos(theta[4])-sin(theta[1])*sin(theta[2])*cos(theta[3])*sin(theta[4]))*
                sin(theta[6])+((sin(theta[1])*cos(theta[2])*sin(theta[4])+sin(theta[1])*sin(theta[2])*cos(theta[3])*cos(theta[4]))*cos(theta[5])-sin(theta[1])*sin(theta[2])*
                sin(theta[3])*sin(theta[5]))*cos(theta[6]))*cos(theta[7]))*sin(theta[8])+((
                                                                                              (cos(theta[1])*cos(theta[2])*sin(theta[4])+cos(theta[1])*sin(theta[2])*cos(theta[3])*cos(theta[4]))*sin(theta[5])+cos(theta[1])*sin(theta[2])*sin(theta[3])*
                cos(theta[5]))*sin(theta[7])+((cos(theta[1])*sin(theta[2])*cos(theta[3])*sin(theta[4])-cos(theta[1])*cos(theta[2])*cos(theta[4]))*sin(theta[6])+(
                    cos(theta[1])*sin(theta[2])*sin(theta[3])*sin(theta[5])+(-cos(theta[1])*cos(theta[2])*sin(theta[4])-cos(theta[1])*sin(theta[2])*cos(theta[3])*cos(theta[4]))*
                cos(theta[5]))*cos(theta[6]))*cos(theta[7]))*cos(theta[8]))*r11)*r31+(
                    ((sin(theta[2])*sin(theta[4])-cos(theta[2])*cos(theta[3])*cos(theta[4]))*sin(theta[5])-cos(theta[2])*sin(theta[3])*cos(theta[5]))*sin(theta[7])+(
                    (-cos(theta[2])*cos(theta[3])*sin(theta[4])-sin(theta[2])*cos(theta[4]))*sin(theta[6])+
                ((cos(theta[2])*cos(theta[3])*cos(theta[4])-sin(theta[2])*sin(theta[4]))*cos(theta[5])-cos(theta[2])*sin(theta[3])*sin(theta[5]))*cos(theta[6]))*cos(theta[7])
                )*r21*r21+(((sin(theta[2])*sin(theta[4])-cos(theta[2])*cos(theta[3])*cos(theta[4]))*sin(theta[5])-cos(theta[2])*sin(theta[3])*cos(theta[5]))*sin(theta[7])+(
                    (-cos(theta[2])*cos(theta[3])*sin(theta[4])-sin(theta[2])*cos(theta[4]))*sin(theta[6])+
                ((cos(theta[2])*cos(theta[3])*cos(theta[4])-sin(theta[2])*sin(theta[4]))*cos(theta[5])-cos(theta[2])*sin(theta[3])*sin(theta[5]))*cos(theta[6]))*cos(theta[7])
                )*r11*r11)/(sqrt(r21*r21+r11*r11)*(r31*r31+r21*r21+r11*r11));


        Jw.at<double>(0,2) = ((((((cos(theta[1])*cos(theta[2])*sin(theta[3])+sin(theta[1])*cos(theta[3]))*cos(theta[4])*sin(theta[5])+
                (sin(theta[1])*sin(theta[3])-cos(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[5]))*sin(theta[7])+(
                    (cos(theta[1])*cos(theta[2])*sin(theta[3])+sin(theta[1])*cos(theta[3]))*sin(theta[4])*sin(theta[6])+(
                    (sin(theta[1])*sin(theta[3])-cos(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[5])+
                (-cos(theta[1])*cos(theta[2])*sin(theta[3])-sin(theta[1])*cos(theta[3]))*cos(theta[4])*cos(theta[5]))*cos(theta[6]))*cos(theta[7]))*sin(theta[8])+((
                                                                                                                                                                       (sin(theta[1])*cos(theta[2])*sin(theta[3])-cos(theta[1])*cos(theta[3]))*cos(theta[4])*sin(theta[5])+
                (-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[5]))*sin(theta[7])+(
                    (sin(theta[1])*cos(theta[2])*sin(theta[3])-cos(theta[1])*cos(theta[3]))*sin(theta[4])*sin(theta[6])+(
                    (-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[5])+
                (cos(theta[1])*cos(theta[3])-sin(theta[1])*cos(theta[2])*sin(theta[3]))*cos(theta[4])*cos(theta[5]))*cos(theta[6]))*cos(theta[7]))*cos(theta[8]))*r21+(((
                                                                                                                                                                            (cos(theta[1])*cos(theta[3])-sin(theta[1])*cos(theta[2])*sin(theta[3]))*cos(theta[4])*sin(theta[5])+
                (cos(theta[1])*sin(theta[3])+sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[5]))*sin(theta[7])+(
                    (cos(theta[1])*cos(theta[3])-sin(theta[1])*cos(theta[2])*sin(theta[3]))*sin(theta[4])*sin(theta[6])+(
                    (cos(theta[1])*sin(theta[3])+sin(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[5])+(sin(theta[1])*cos(theta[2])*sin(theta[3])-cos(theta[1])*cos(theta[3]))
                *cos(theta[4])*cos(theta[5]))*cos(theta[6]))*cos(theta[7]))*sin(theta[8])+(((cos(theta[1])*cos(theta[2])*sin(theta[3])+sin(theta[1])*cos(theta[3]))*
                cos(theta[4])*sin(theta[5])+(sin(theta[1])*sin(theta[3])-cos(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[5]))*sin(theta[7])+(
                    (cos(theta[1])*cos(theta[2])*sin(theta[3])+sin(theta[1])*cos(theta[3]))*sin(theta[4])*sin(theta[6])+(
                    (sin(theta[1])*sin(theta[3])-cos(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[5])+
                (-cos(theta[1])*cos(theta[2])*sin(theta[3])-sin(theta[1])*cos(theta[3]))*cos(theta[4])*cos(theta[5]))*cos(theta[6]))*cos(theta[7]))*cos(theta[8]))*r11)*r31+
                ((sin(theta[2])*sin(theta[3])*cos(theta[4])*sin(theta[5])-sin(theta[2])*cos(theta[3])*cos(theta[5]))*sin(theta[7])+(sin(theta[2])*sin(theta[3])*sin(theta[4])*
                sin(theta[6])+(-sin(theta[2])*cos(theta[3])*sin(theta[5])-sin(theta[2])*sin(theta[3])*cos(theta[4])*cos(theta[5]))*cos(theta[6]))*cos(theta[7]))*r21*r21+(
                    (sin(theta[2])*sin(theta[3])*cos(theta[4])*sin(theta[5])-sin(theta[2])*cos(theta[3])*cos(theta[5]))*sin(theta[7])+(sin(theta[2])*sin(theta[3])*sin(theta[4])*
                sin(theta[6])+(-sin(theta[2])*cos(theta[3])*sin(theta[5])-sin(theta[2])*sin(theta[3])*cos(theta[4])*cos(theta[5]))*cos(theta[6]))*cos(theta[7]))*r11*r11)/(
                    sqrt(r21*r21+r11*r11)*(r31*r31+r21*r21+r11*r11));

        Jw.at<double>(0,3) = -((((((sin(theta[1])*sin(theta[3])-cos(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[4])-cos(theta[1])*sin(theta[2])*cos(theta[4]))*
                sin(theta[5])*sin(theta[7])+(
                    ((cos(theta[1])*cos(theta[2])*cos(theta[3])-sin(theta[1])*sin(theta[3]))*cos(theta[4])-cos(theta[1])*sin(theta[2])*sin(theta[4]))*sin(theta[6])+
                ((cos(theta[1])*cos(theta[2])*cos(theta[3])-sin(theta[1])*sin(theta[3]))*sin(theta[4])+cos(theta[1])*sin(theta[2])*cos(theta[4]))*cos(theta[5])*cos(theta[6]))
                *cos(theta[7]))*sin(theta[8])+(
                    ((-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[4])-sin(theta[1])*sin(theta[2])*cos(theta[4]))*sin(theta[5])*sin(theta[7])
                +(((cos(theta[1])*sin(theta[3])+sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4])-sin(theta[1])*sin(theta[2])*sin(theta[4]))*sin(theta[6])+
                ((cos(theta[1])*sin(theta[3])+sin(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[4])+sin(theta[1])*sin(theta[2])*cos(theta[4]))*cos(theta[5])*cos(theta[6])
                )*cos(theta[7]))*cos(theta[8]))*r21+((
                                                         ((cos(theta[1])*sin(theta[3])+sin(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[4])+sin(theta[1])*sin(theta[2])*cos(theta[4]))*sin(theta[5])*sin(theta[7])
                +((sin(theta[1])*sin(theta[2])*sin(theta[4])+(-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*sin(theta[6])+
                ((-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[4])-sin(theta[1])*sin(theta[2])*cos(theta[4]))*cos(theta[5])*cos(theta[6])
                )*cos(theta[7]))*sin(theta[8])+(
                    ((sin(theta[1])*sin(theta[3])-cos(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[4])-cos(theta[1])*sin(theta[2])*cos(theta[4]))*sin(theta[5])*sin(theta[7])+
                (((cos(theta[1])*cos(theta[2])*cos(theta[3])-sin(theta[1])*sin(theta[3]))*cos(theta[4])-cos(theta[1])*sin(theta[2])*sin(theta[4]))*sin(theta[6])+
                ((cos(theta[1])*cos(theta[2])*cos(theta[3])-sin(theta[1])*sin(theta[3]))*sin(theta[4])+cos(theta[1])*sin(theta[2])*cos(theta[4]))*cos(theta[5])*cos(theta[6]))
                *cos(theta[7]))*cos(theta[8]))*r11)*r31+((cos(theta[2])*cos(theta[4])-sin(theta[2])*cos(theta[3])*sin(theta[4]))*sin(theta[5])*sin(theta[7])+(
                    (cos(theta[2])*sin(theta[4])+sin(theta[2])*cos(theta[3])*cos(theta[4]))*sin(theta[6])+(sin(theta[2])*cos(theta[3])*sin(theta[4])-cos(theta[2])*cos(theta[4]))
                *cos(theta[5])*cos(theta[6]))*cos(theta[7]))*r21*r21+((cos(theta[2])*cos(theta[4])-sin(theta[2])*cos(theta[3])*sin(theta[4]))*sin(theta[5])*sin(theta[7])+(
                    (cos(theta[2])*sin(theta[4])+sin(theta[2])*cos(theta[3])*cos(theta[4]))*sin(theta[6])+(sin(theta[2])*cos(theta[3])*sin(theta[4])-cos(theta[2])*cos(theta[4]))
                *cos(theta[5])*cos(theta[6]))*cos(theta[7]))*r11*r11)/(sqrt(r21*r21+r11*r11)*(r31*r31+r21*r21+r11*r11));

        Jw.at<double>(0,4) = ((((((cos(theta[1])*cos(theta[2])*sin(theta[3])+sin(theta[1])*cos(theta[3]))*sin(theta[5])+
                (cos(theta[1])*sin(theta[2])*sin(theta[4])+(sin(theta[1])*sin(theta[3])-cos(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*cos(theta[5]))*sin(theta[7])
                +((cos(theta[1])*sin(theta[2])*sin(theta[4])+(sin(theta[1])*sin(theta[3])-cos(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*sin(theta[5])+
                (-cos(theta[1])*cos(theta[2])*sin(theta[3])-sin(theta[1])*cos(theta[3]))*cos(theta[5]))*cos(theta[6])*cos(theta[7]))*sin(theta[8])+((
                                                                                                                                                        (sin(theta[1])*cos(theta[2])*sin(theta[3])-cos(theta[1])*cos(theta[3]))*sin(theta[5])+
                (sin(theta[1])*sin(theta[2])*sin(theta[4])+(-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*cos(theta[5]))*
                sin(theta[7])+((sin(theta[1])*sin(theta[2])*sin(theta[4])+(-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*
                sin(theta[5])+(cos(theta[1])*cos(theta[3])-sin(theta[1])*cos(theta[2])*sin(theta[3]))*cos(theta[5]))*cos(theta[6])*cos(theta[7]))*cos(theta[8]))*r21+(((
                                                                                                                                                                           (cos(theta[1])*cos(theta[3])-sin(theta[1])*cos(theta[2])*sin(theta[3]))*sin(theta[5])+
                ((cos(theta[1])*sin(theta[3])+sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4])-sin(theta[1])*sin(theta[2])*sin(theta[4]))*cos(theta[5]))*sin(theta[7])
                +(((cos(theta[1])*sin(theta[3])+sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4])-sin(theta[1])*sin(theta[2])*sin(theta[4]))*sin(theta[5])+
                (sin(theta[1])*cos(theta[2])*sin(theta[3])-cos(theta[1])*cos(theta[3]))*cos(theta[5]))*cos(theta[6])*cos(theta[7]))*sin(theta[8])+((
                                                                                                                                                       (cos(theta[1])*cos(theta[2])*sin(theta[3])+sin(theta[1])*cos(theta[3]))*sin(theta[5])+
                (cos(theta[1])*sin(theta[2])*sin(theta[4])+(sin(theta[1])*sin(theta[3])-cos(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*cos(theta[5]))*sin(theta[7])
                +((cos(theta[1])*sin(theta[2])*sin(theta[4])+(sin(theta[1])*sin(theta[3])-cos(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*sin(theta[5])+
                (-cos(theta[1])*cos(theta[2])*sin(theta[3])-sin(theta[1])*cos(theta[3]))*cos(theta[5]))*cos(theta[6])*cos(theta[7]))*cos(theta[8]))*r11)*r31+(
                    (sin(theta[2])*sin(theta[3])*sin(theta[5])+(-cos(theta[2])*sin(theta[4])-sin(theta[2])*cos(theta[3])*cos(theta[4]))*cos(theta[5]))*sin(theta[7])+
                ((-cos(theta[2])*sin(theta[4])-sin(theta[2])*cos(theta[3])*cos(theta[4]))*sin(theta[5])-sin(theta[2])*sin(theta[3])*cos(theta[5]))*cos(theta[6])*cos(theta[7])
                )*r21*r21+((sin(theta[2])*sin(theta[3])*sin(theta[5])+(-cos(theta[2])*sin(theta[4])-sin(theta[2])*cos(theta[3])*cos(theta[4]))*cos(theta[5]))*sin(theta[7])+
                ((-cos(theta[2])*sin(theta[4])-sin(theta[2])*cos(theta[3])*cos(theta[4]))*sin(theta[5])-sin(theta[2])*sin(theta[3])*cos(theta[5]))*cos(theta[6])*cos(theta[7])
                )*r11*r11)/(sqrt(r21*r21+r11*r11)*(r31*r31+r21*r21+r11*r11));


        Jw.at<double>(0,5) = ((((((cos(theta[1])*cos(theta[2])*sin(theta[3])+sin(theta[1])*cos(theta[3]))*sin(theta[5])+
                (cos(theta[1])*sin(theta[2])*sin(theta[4])+(sin(theta[1])*sin(theta[3])-cos(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*cos(theta[5]))*sin(theta[6])
                +((sin(theta[1])*sin(theta[3])-cos(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[4])-cos(theta[1])*sin(theta[2])*cos(theta[4]))*cos(theta[6]))*
                cos(theta[7])*sin(theta[8])+(((sin(theta[1])*cos(theta[2])*sin(theta[3])-cos(theta[1])*cos(theta[3]))*sin(theta[5])+
                (sin(theta[1])*sin(theta[2])*sin(theta[4])+(-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*cos(theta[5]))*
                sin(theta[6])+((-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[4])-sin(theta[1])*sin(theta[2])*cos(theta[4]))*
                cos(theta[6]))*cos(theta[7])*cos(theta[8]))*r21+((((cos(theta[1])*cos(theta[3])-sin(theta[1])*cos(theta[2])*sin(theta[3]))*sin(theta[5])+
                ((cos(theta[1])*sin(theta[3])+sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4])-sin(theta[1])*sin(theta[2])*sin(theta[4]))*cos(theta[5]))*sin(theta[6])
                +((cos(theta[1])*sin(theta[3])+sin(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[4])+sin(theta[1])*sin(theta[2])*cos(theta[4]))*cos(theta[6]))*
                cos(theta[7])*sin(theta[8])+(((cos(theta[1])*cos(theta[2])*sin(theta[3])+sin(theta[1])*cos(theta[3]))*sin(theta[5])+
                (cos(theta[1])*sin(theta[2])*sin(theta[4])+(sin(theta[1])*sin(theta[3])-cos(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*cos(theta[5]))*sin(theta[6])
                +((sin(theta[1])*sin(theta[3])-cos(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[4])-cos(theta[1])*sin(theta[2])*cos(theta[4]))*cos(theta[6]))*
                cos(theta[7])*cos(theta[8]))*r11)*r31+(
                    (sin(theta[2])*sin(theta[3])*sin(theta[5])+(-cos(theta[2])*sin(theta[4])-sin(theta[2])*cos(theta[3])*cos(theta[4]))*cos(theta[5]))*sin(theta[6])+
                (cos(theta[2])*cos(theta[4])-sin(theta[2])*cos(theta[3])*sin(theta[4]))*cos(theta[6]))*cos(theta[7])*r21*r21+(
                    (sin(theta[2])*sin(theta[3])*sin(theta[5])+(-cos(theta[2])*sin(theta[4])-sin(theta[2])*cos(theta[3])*cos(theta[4]))*cos(theta[5]))*sin(theta[6])+
                (cos(theta[2])*cos(theta[4])-sin(theta[2])*cos(theta[3])*sin(theta[4]))*cos(theta[6]))*cos(theta[7])*r11*r11)/(sqrt(r21*r21+r11*r11)*(r31*r31+r21*r21+r11*r11));

        Jw.at<double>(0,6) = -(((((((sin(theta[1])*sin(theta[3])-cos(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[4])-cos(theta[1])*sin(theta[2])*cos(theta[4]))*
                sin(theta[6])+((-cos(theta[1])*cos(theta[2])*sin(theta[3])-sin(theta[1])*cos(theta[3]))*sin(theta[5])+
                ((cos(theta[1])*cos(theta[2])*cos(theta[3])-sin(theta[1])*sin(theta[3]))*cos(theta[4])-cos(theta[1])*sin(theta[2])*sin(theta[4]))*cos(theta[5]))*cos(theta[6])
                )*sin(theta[7])+(((cos(theta[1])*cos(theta[2])*cos(theta[3])-sin(theta[1])*sin(theta[3]))*cos(theta[4])-cos(theta[1])*sin(theta[2])*sin(theta[4]))*
                sin(theta[5])+(cos(theta[1])*cos(theta[2])*sin(theta[3])+sin(theta[1])*cos(theta[3]))*cos(theta[5]))*cos(theta[7]))*sin(theta[8])+((
                                                                                                                                                       ((-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[4])-sin(theta[1])*sin(theta[2])*cos(theta[4]))*sin(theta[6])+(
                    (cos(theta[1])*cos(theta[3])-sin(theta[1])*cos(theta[2])*sin(theta[3]))*sin(theta[5])+
                ((cos(theta[1])*sin(theta[3])+sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4])-sin(theta[1])*sin(theta[2])*sin(theta[4]))*cos(theta[5]))*cos(theta[6])
                )*sin(theta[7])+(((cos(theta[1])*sin(theta[3])+sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4])-sin(theta[1])*sin(theta[2])*sin(theta[4]))*
                sin(theta[5])+(sin(theta[1])*cos(theta[2])*sin(theta[3])-cos(theta[1])*cos(theta[3]))*cos(theta[5]))*cos(theta[7]))*cos(theta[8]))*r21+(((
                                                                                                                                                             ((cos(theta[1])*sin(theta[3])+sin(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[4])+sin(theta[1])*sin(theta[2])*cos(theta[4]))*sin(theta[6])+(
                    (sin(theta[1])*cos(theta[2])*sin(theta[3])-cos(theta[1])*cos(theta[3]))*sin(theta[5])+
                (sin(theta[1])*sin(theta[2])*sin(theta[4])+(-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*cos(theta[5]))*
                cos(theta[6]))*sin(theta[7])+(
                    (sin(theta[1])*sin(theta[2])*sin(theta[4])+(-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*sin(theta[5])+
                (cos(theta[1])*cos(theta[3])-sin(theta[1])*cos(theta[2])*sin(theta[3]))*cos(theta[5]))*cos(theta[7]))*sin(theta[8])+((
                                                                                                                                         ((sin(theta[1])*sin(theta[3])-cos(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[4])-cos(theta[1])*sin(theta[2])*cos(theta[4]))*sin(theta[6])+(
                    (-cos(theta[1])*cos(theta[2])*sin(theta[3])-sin(theta[1])*cos(theta[3]))*sin(theta[5])+
                ((cos(theta[1])*cos(theta[2])*cos(theta[3])-sin(theta[1])*sin(theta[3]))*cos(theta[4])-cos(theta[1])*sin(theta[2])*sin(theta[4]))*cos(theta[5]))*cos(theta[6])
                )*sin(theta[7])+(((cos(theta[1])*cos(theta[2])*cos(theta[3])-sin(theta[1])*sin(theta[3]))*cos(theta[4])-cos(theta[1])*sin(theta[2])*sin(theta[4]))*
                sin(theta[5])+(cos(theta[1])*cos(theta[2])*sin(theta[3])+sin(theta[1])*cos(theta[3]))*cos(theta[5]))*cos(theta[7]))*cos(theta[8]))*r11)*r31+((
                                                                                                                                                                 (cos(theta[2])*cos(theta[4])-sin(theta[2])*cos(theta[3])*sin(theta[4]))*sin(theta[6])+
                ((cos(theta[2])*sin(theta[4])+sin(theta[2])*cos(theta[3])*cos(theta[4]))*cos(theta[5])-sin(theta[2])*sin(theta[3])*sin(theta[5]))*cos(theta[6]))*sin(theta[7])
                +((cos(theta[2])*sin(theta[4])+sin(theta[2])*cos(theta[3])*cos(theta[4]))*sin(theta[5])+sin(theta[2])*sin(theta[3])*cos(theta[5]))*cos(theta[7]))*r21*r21+((
                                                                                                                                                                             (cos(theta[2])*cos(theta[4])-sin(theta[2])*cos(theta[3])*sin(theta[4]))*sin(theta[6])+
                ((cos(theta[2])*sin(theta[4])+sin(theta[2])*cos(theta[3])*cos(theta[4]))*cos(theta[5])-sin(theta[2])*sin(theta[3])*sin(theta[5]))*cos(theta[6]))*sin(theta[7])
                +((cos(theta[2])*sin(theta[4])+sin(theta[2])*cos(theta[3])*cos(theta[4]))*sin(theta[5])+sin(theta[2])*sin(theta[3])*cos(theta[5]))*cos(theta[7]))*r11*r11)/(
                    sqrt(r21*r21+r11*r11)*(r31*r31+r21*r21+r11*r11));

        Jw.at<double>(0,7) = ((r11*r21+(((((cos(theta[1])*cos(theta[2])*cos(theta[3])-sin(theta[1])*sin(theta[3]))*cos(theta[4])-cos(theta[1])*sin(theta[2])*sin(theta[4]))*
                sin(theta[5])+(cos(theta[1])*cos(theta[2])*sin(theta[3])+sin(theta[1])*cos(theta[3]))*cos(theta[5]))*sin(theta[7])+(
                    ((cos(theta[1])*cos(theta[2])*cos(theta[3])-sin(theta[1])*sin(theta[3]))*sin(theta[4])+cos(theta[1])*sin(theta[2])*cos(theta[4]))*sin(theta[6])+(
                    (cos(theta[1])*cos(theta[2])*sin(theta[3])+sin(theta[1])*cos(theta[3]))*sin(theta[5])+
                (cos(theta[1])*sin(theta[2])*sin(theta[4])+(sin(theta[1])*sin(theta[3])-cos(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*cos(theta[5]))*cos(theta[6])
                )*cos(theta[7]))*sin(theta[8])+((
                                                    ((cos(theta[1])*sin(theta[3])+sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4])-sin(theta[1])*sin(theta[2])*sin(theta[4]))*sin(theta[5])+
                (sin(theta[1])*cos(theta[2])*sin(theta[3])-cos(theta[1])*cos(theta[3]))*cos(theta[5]))*sin(theta[7])+(
                    ((cos(theta[1])*sin(theta[3])+sin(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[4])+sin(theta[1])*sin(theta[2])*cos(theta[4]))*sin(theta[6])+(
                    (sin(theta[1])*cos(theta[2])*sin(theta[3])-cos(theta[1])*cos(theta[3]))*sin(theta[5])+
                (sin(theta[1])*sin(theta[2])*sin(theta[4])+(-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*cos(theta[5]))*
                cos(theta[6]))*cos(theta[7]))*cos(theta[8]))*r11)*r31)/(sqrt(r21*r21+r11*r11)*(r31*r31+r21*r21+r11*r11));


        Jw.at<double>(0,8) = 0.0;

        Jw.at<double>(0,9) = 0.0;


        //----------------

        Jw.at<double>(1,0) = (((((cos(theta[1])*sin(theta[2])*sin(theta[4])+(sin(theta[1])*sin(theta[3])-cos(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*sin(theta[5])
                +(-cos(theta[1])*cos(theta[2])*sin(theta[3])-sin(theta[1])*cos(theta[3]))*cos(theta[5]))*sin(theta[7])+(
                    ((sin(theta[1])*sin(theta[3])-cos(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[4])-cos(theta[1])*sin(theta[2])*cos(theta[4]))*sin(theta[6])+(
                    (-cos(theta[1])*cos(theta[2])*sin(theta[3])-sin(theta[1])*cos(theta[3]))*sin(theta[5])+
                ((cos(theta[1])*cos(theta[2])*cos(theta[3])-sin(theta[1])*sin(theta[3]))*cos(theta[4])-cos(theta[1])*sin(theta[2])*sin(theta[4]))*cos(theta[5]))*cos(theta[6])
                )*cos(theta[7]))*sin(theta[8])+((
                                                    (sin(theta[1])*sin(theta[2])*sin(theta[4])+(-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*sin(theta[5])+
                (cos(theta[1])*cos(theta[3])-sin(theta[1])*cos(theta[2])*sin(theta[3]))*cos(theta[5]))*sin(theta[7])+(
                    ((-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[4])-sin(theta[1])*sin(theta[2])*cos(theta[4]))*sin(theta[6])+(
                    (cos(theta[1])*cos(theta[3])-sin(theta[1])*cos(theta[2])*sin(theta[3]))*sin(theta[5])+
                ((cos(theta[1])*sin(theta[3])+sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4])-sin(theta[1])*sin(theta[2])*sin(theta[4]))*cos(theta[5]))*cos(theta[6])
                )*cos(theta[7]))*cos(theta[8]))*r21+(((
                                                          ((cos(theta[1])*sin(theta[3])+sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4])-sin(theta[1])*sin(theta[2])*sin(theta[4]))*sin(theta[5])+
                (sin(theta[1])*cos(theta[2])*sin(theta[3])-cos(theta[1])*cos(theta[3]))*cos(theta[5]))*sin(theta[7])+(
                    ((cos(theta[1])*sin(theta[3])+sin(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[4])+sin(theta[1])*sin(theta[2])*cos(theta[4]))*sin(theta[6])+(
                    (sin(theta[1])*cos(theta[2])*sin(theta[3])-cos(theta[1])*cos(theta[3]))*sin(theta[5])+
                (sin(theta[1])*sin(theta[2])*sin(theta[4])+(-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*cos(theta[5]))*
                cos(theta[6]))*cos(theta[7]))*sin(theta[8])+((
                                                                 (cos(theta[1])*sin(theta[2])*sin(theta[4])+(sin(theta[1])*sin(theta[3])-cos(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*sin(theta[5])+
                (-cos(theta[1])*cos(theta[2])*sin(theta[3])-sin(theta[1])*cos(theta[3]))*cos(theta[5]))*sin(theta[7])+(
                    ((sin(theta[1])*sin(theta[3])-cos(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[4])-cos(theta[1])*sin(theta[2])*cos(theta[4]))*sin(theta[6])+(
                    (-cos(theta[1])*cos(theta[2])*sin(theta[3])-sin(theta[1])*cos(theta[3]))*sin(theta[5])+
                ((cos(theta[1])*cos(theta[2])*cos(theta[3])-sin(theta[1])*sin(theta[3]))*cos(theta[4])-cos(theta[1])*sin(theta[2])*sin(theta[4]))*cos(theta[5]))*cos(theta[6])
                )*cos(theta[7]))*cos(theta[8]))*r11)/(r21*r21+r11*r11);

        Jw.at<double>(1,1) = (((((sin(theta[1])*cos(theta[2])*sin(theta[4])+sin(theta[1])*sin(theta[2])*cos(theta[3])*cos(theta[4]))*sin(theta[5])+sin(theta[1])*sin(theta[2])*
                sin(theta[3])*cos(theta[5]))*sin(theta[7])+((sin(theta[1])*sin(theta[2])*cos(theta[3])*sin(theta[4])-sin(theta[1])*cos(theta[2])*cos(theta[4]))*sin(theta[6])
                +(sin(theta[1])*sin(theta[2])*sin(theta[3])*sin(theta[5])+
                (-sin(theta[1])*cos(theta[2])*sin(theta[4])-sin(theta[1])*sin(theta[2])*cos(theta[3])*cos(theta[4]))*cos(theta[5]))*cos(theta[6]))*cos(theta[7]))*
                sin(theta[8])+(((-cos(theta[1])*cos(theta[2])*sin(theta[4])-cos(theta[1])*sin(theta[2])*cos(theta[3])*cos(theta[4]))*sin(theta[5])-cos(theta[1])*
                sin(theta[2])*sin(theta[3])*cos(theta[5]))*sin(theta[7])+((cos(theta[1])*cos(theta[2])*cos(theta[4])-cos(theta[1])*sin(theta[2])*cos(theta[3])*sin(theta[4]))*
                sin(theta[6])+((cos(theta[1])*cos(theta[2])*sin(theta[4])+cos(theta[1])*sin(theta[2])*cos(theta[3])*cos(theta[4]))*cos(theta[5])-cos(theta[1])*sin(theta[2])*
                sin(theta[3])*sin(theta[5]))*cos(theta[6]))*cos(theta[7]))*cos(theta[8]))*r21+(((
                                                                                                    (cos(theta[1])*cos(theta[2])*sin(theta[4])+cos(theta[1])*sin(theta[2])*cos(theta[3])*cos(theta[4]))*sin(theta[5])+cos(theta[1])*sin(theta[2])*sin(theta[3])*
                cos(theta[5]))*sin(theta[7])+((cos(theta[1])*sin(theta[2])*cos(theta[3])*sin(theta[4])-cos(theta[1])*cos(theta[2])*cos(theta[4]))*sin(theta[6])+(
                    cos(theta[1])*sin(theta[2])*sin(theta[3])*sin(theta[5])+(-cos(theta[1])*cos(theta[2])*sin(theta[4])-cos(theta[1])*sin(theta[2])*cos(theta[3])*cos(theta[4]))*
                cos(theta[5]))*cos(theta[6]))*cos(theta[7]))*sin(theta[8])+((
                                                                                (sin(theta[1])*cos(theta[2])*sin(theta[4])+sin(theta[1])*sin(theta[2])*cos(theta[3])*cos(theta[4]))*sin(theta[5])+sin(theta[1])*sin(theta[2])*sin(theta[3])*
                cos(theta[5]))*sin(theta[7])+((sin(theta[1])*sin(theta[2])*cos(theta[3])*sin(theta[4])-sin(theta[1])*cos(theta[2])*cos(theta[4]))*sin(theta[6])+(
                    sin(theta[1])*sin(theta[2])*sin(theta[3])*sin(theta[5])+(-sin(theta[1])*cos(theta[2])*sin(theta[4])-sin(theta[1])*sin(theta[2])*cos(theta[3])*cos(theta[4]))*
                cos(theta[5]))*cos(theta[6]))*cos(theta[7]))*cos(theta[8]))*r11)/(r21*r21+r11*r11);


        Jw.at<double>(1,2) = (((((sin(theta[1])*cos(theta[2])*sin(theta[3])-cos(theta[1])*cos(theta[3]))*cos(theta[4])*sin(theta[5])+
                (-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[5]))*sin(theta[7])+(
                    (sin(theta[1])*cos(theta[2])*sin(theta[3])-cos(theta[1])*cos(theta[3]))*sin(theta[4])*sin(theta[6])+(
                    (-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[5])+
                (cos(theta[1])*cos(theta[3])-sin(theta[1])*cos(theta[2])*sin(theta[3]))*cos(theta[4])*cos(theta[5]))*cos(theta[6]))*cos(theta[7]))*sin(theta[8])+((
                                                                                                                                                                      (-cos(theta[1])*cos(theta[2])*sin(theta[3])-sin(theta[1])*cos(theta[3]))*cos(theta[4])*sin(theta[5])+
                (cos(theta[1])*cos(theta[2])*cos(theta[3])-sin(theta[1])*sin(theta[3]))*cos(theta[5]))*sin(theta[7])+(
                    (-cos(theta[1])*cos(theta[2])*sin(theta[3])-sin(theta[1])*cos(theta[3]))*sin(theta[4])*sin(theta[6])+(
                    (cos(theta[1])*cos(theta[2])*cos(theta[3])-sin(theta[1])*sin(theta[3]))*sin(theta[5])+(cos(theta[1])*cos(theta[2])*sin(theta[3])+sin(theta[1])*cos(theta[3]))
                *cos(theta[4])*cos(theta[5]))*cos(theta[6]))*cos(theta[7]))*cos(theta[8]))*r21+((((cos(theta[1])*cos(theta[2])*sin(theta[3])+sin(theta[1])*cos(theta[3]))*
                cos(theta[4])*sin(theta[5])+(sin(theta[1])*sin(theta[3])-cos(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[5]))*sin(theta[7])+(
                    (cos(theta[1])*cos(theta[2])*sin(theta[3])+sin(theta[1])*cos(theta[3]))*sin(theta[4])*sin(theta[6])+(
                    (sin(theta[1])*sin(theta[3])-cos(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[5])+
                (-cos(theta[1])*cos(theta[2])*sin(theta[3])-sin(theta[1])*cos(theta[3]))*cos(theta[4])*cos(theta[5]))*cos(theta[6]))*cos(theta[7]))*sin(theta[8])+((
                                                                                                                                                                       (sin(theta[1])*cos(theta[2])*sin(theta[3])-cos(theta[1])*cos(theta[3]))*cos(theta[4])*sin(theta[5])+
                (-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[5]))*sin(theta[7])+(
                    (sin(theta[1])*cos(theta[2])*sin(theta[3])-cos(theta[1])*cos(theta[3]))*sin(theta[4])*sin(theta[6])+(
                    (-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[5])+
                (cos(theta[1])*cos(theta[3])-sin(theta[1])*cos(theta[2])*sin(theta[3]))*cos(theta[4])*cos(theta[5]))*cos(theta[6]))*cos(theta[7]))*cos(theta[8]))*r11)/(
                    r21*r21+r11*r11);


        Jw.at<double>(1,3) = (((((cos(theta[1])*sin(theta[3])+sin(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[4])+sin(theta[1])*sin(theta[2])*cos(theta[4]))*sin(theta[5])*
                sin(theta[7])+((sin(theta[1])*sin(theta[2])*sin(theta[4])+(-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*
                sin(theta[6])+((-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[4])-sin(theta[1])*sin(theta[2])*cos(theta[4]))*
                cos(theta[5])*cos(theta[6]))*cos(theta[7]))*sin(theta[8])+(
                    ((sin(theta[1])*sin(theta[3])-cos(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[4])-cos(theta[1])*sin(theta[2])*cos(theta[4]))*sin(theta[5])*sin(theta[7])+
                (((cos(theta[1])*cos(theta[2])*cos(theta[3])-sin(theta[1])*sin(theta[3]))*cos(theta[4])-cos(theta[1])*sin(theta[2])*sin(theta[4]))*sin(theta[6])+
                ((cos(theta[1])*cos(theta[2])*cos(theta[3])-sin(theta[1])*sin(theta[3]))*sin(theta[4])+cos(theta[1])*sin(theta[2])*cos(theta[4]))*cos(theta[5])*cos(theta[6]))
                *cos(theta[7]))*cos(theta[8]))*r21+((
                                                        ((cos(theta[1])*cos(theta[2])*cos(theta[3])-sin(theta[1])*sin(theta[3]))*sin(theta[4])+cos(theta[1])*sin(theta[2])*cos(theta[4]))*sin(theta[5])*sin(theta[7])
                +((cos(theta[1])*sin(theta[2])*sin(theta[4])+(sin(theta[1])*sin(theta[3])-cos(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*sin(theta[6])+
                ((sin(theta[1])*sin(theta[3])-cos(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[4])-cos(theta[1])*sin(theta[2])*cos(theta[4]))*cos(theta[5])*cos(theta[6]))
                *cos(theta[7]))*sin(theta[8])+(
                    ((cos(theta[1])*sin(theta[3])+sin(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[4])+sin(theta[1])*sin(theta[2])*cos(theta[4]))*sin(theta[5])*sin(theta[7])
                +((sin(theta[1])*sin(theta[2])*sin(theta[4])+(-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*sin(theta[6])+
                ((-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[4])-sin(theta[1])*sin(theta[2])*cos(theta[4]))*cos(theta[5])*cos(theta[6])
                )*cos(theta[7]))*cos(theta[8]))*r11)/(r21*r21+r11*r11);


        Jw.at<double>(1,4) = (((((sin(theta[1])*cos(theta[2])*sin(theta[3])-cos(theta[1])*cos(theta[3]))*sin(theta[5])+
                (sin(theta[1])*sin(theta[2])*sin(theta[4])+(-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*cos(theta[5]))*
                sin(theta[7])+((sin(theta[1])*sin(theta[2])*sin(theta[4])+(-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*
                sin(theta[5])+(cos(theta[1])*cos(theta[3])-sin(theta[1])*cos(theta[2])*sin(theta[3]))*cos(theta[5]))*cos(theta[6])*cos(theta[7]))*sin(theta[8])+((
                                                                                                                                                                     (-cos(theta[1])*cos(theta[2])*sin(theta[3])-sin(theta[1])*cos(theta[3]))*sin(theta[5])+
                ((cos(theta[1])*cos(theta[2])*cos(theta[3])-sin(theta[1])*sin(theta[3]))*cos(theta[4])-cos(theta[1])*sin(theta[2])*sin(theta[4]))*cos(theta[5]))*sin(theta[7])
                +(((cos(theta[1])*cos(theta[2])*cos(theta[3])-sin(theta[1])*sin(theta[3]))*cos(theta[4])-cos(theta[1])*sin(theta[2])*sin(theta[4]))*sin(theta[5])+
                (cos(theta[1])*cos(theta[2])*sin(theta[3])+sin(theta[1])*cos(theta[3]))*cos(theta[5]))*cos(theta[6])*cos(theta[7]))*cos(theta[8]))*r21+(((
                                                                                                                                                             (cos(theta[1])*cos(theta[2])*sin(theta[3])+sin(theta[1])*cos(theta[3]))*sin(theta[5])+
                (cos(theta[1])*sin(theta[2])*sin(theta[4])+(sin(theta[1])*sin(theta[3])-cos(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*cos(theta[5]))*sin(theta[7])
                +((cos(theta[1])*sin(theta[2])*sin(theta[4])+(sin(theta[1])*sin(theta[3])-cos(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*sin(theta[5])+
                (-cos(theta[1])*cos(theta[2])*sin(theta[3])-sin(theta[1])*cos(theta[3]))*cos(theta[5]))*cos(theta[6])*cos(theta[7]))*sin(theta[8])+((
                                                                                                                                                        (sin(theta[1])*cos(theta[2])*sin(theta[3])-cos(theta[1])*cos(theta[3]))*sin(theta[5])+
                (sin(theta[1])*sin(theta[2])*sin(theta[4])+(-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*cos(theta[5]))*
                sin(theta[7])+((sin(theta[1])*sin(theta[2])*sin(theta[4])+(-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*
                sin(theta[5])+(cos(theta[1])*cos(theta[3])-sin(theta[1])*cos(theta[2])*sin(theta[3]))*cos(theta[5]))*cos(theta[6])*cos(theta[7]))*cos(theta[8]))*r11)/(
                    r21*r21+r11*r11);


        Jw.at<double>(1,5) = (((((sin(theta[1])*cos(theta[2])*sin(theta[3])-cos(theta[1])*cos(theta[3]))*sin(theta[5])+
                (sin(theta[1])*sin(theta[2])*sin(theta[4])+(-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*cos(theta[5]))*
                sin(theta[6])+((-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[4])-sin(theta[1])*sin(theta[2])*cos(theta[4]))*
                cos(theta[6]))*cos(theta[7])*sin(theta[8])+(((-cos(theta[1])*cos(theta[2])*sin(theta[3])-sin(theta[1])*cos(theta[3]))*sin(theta[5])+
                ((cos(theta[1])*cos(theta[2])*cos(theta[3])-sin(theta[1])*sin(theta[3]))*cos(theta[4])-cos(theta[1])*sin(theta[2])*sin(theta[4]))*cos(theta[5]))*sin(theta[6])
                +((cos(theta[1])*cos(theta[2])*cos(theta[3])-sin(theta[1])*sin(theta[3]))*sin(theta[4])+cos(theta[1])*sin(theta[2])*cos(theta[4]))*cos(theta[6]))*
                cos(theta[7])*cos(theta[8]))*r21+((((cos(theta[1])*cos(theta[2])*sin(theta[3])+sin(theta[1])*cos(theta[3]))*sin(theta[5])+
                (cos(theta[1])*sin(theta[2])*sin(theta[4])+(sin(theta[1])*sin(theta[3])-cos(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*cos(theta[5]))*sin(theta[6])
                +((sin(theta[1])*sin(theta[3])-cos(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[4])-cos(theta[1])*sin(theta[2])*cos(theta[4]))*cos(theta[6]))*
                cos(theta[7])*sin(theta[8])+(((sin(theta[1])*cos(theta[2])*sin(theta[3])-cos(theta[1])*cos(theta[3]))*sin(theta[5])+
                (sin(theta[1])*sin(theta[2])*sin(theta[4])+(-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*cos(theta[5]))*
                sin(theta[6])+((-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[4])-sin(theta[1])*sin(theta[2])*cos(theta[4]))*
                cos(theta[6]))*cos(theta[7])*cos(theta[8]))*r11)/(r21*r21+r11*r11);

        Jw.at<double>(1,6) = ((((((cos(theta[1])*sin(theta[3])+sin(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[4])+sin(theta[1])*sin(theta[2])*cos(theta[4]))*sin(theta[6])
                +((sin(theta[1])*cos(theta[2])*sin(theta[3])-cos(theta[1])*cos(theta[3]))*sin(theta[5])+
                (sin(theta[1])*sin(theta[2])*sin(theta[4])+(-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*cos(theta[5]))*
                cos(theta[6]))*sin(theta[7])+(
                    (sin(theta[1])*sin(theta[2])*sin(theta[4])+(-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*sin(theta[5])+
                (cos(theta[1])*cos(theta[3])-sin(theta[1])*cos(theta[2])*sin(theta[3]))*cos(theta[5]))*cos(theta[7]))*sin(theta[8])+((
                                                                                                                                         ((sin(theta[1])*sin(theta[3])-cos(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[4])-cos(theta[1])*sin(theta[2])*cos(theta[4]))*sin(theta[6])+(
                    (-cos(theta[1])*cos(theta[2])*sin(theta[3])-sin(theta[1])*cos(theta[3]))*sin(theta[5])+
                ((cos(theta[1])*cos(theta[2])*cos(theta[3])-sin(theta[1])*sin(theta[3]))*cos(theta[4])-cos(theta[1])*sin(theta[2])*sin(theta[4]))*cos(theta[5]))*cos(theta[6])
                )*sin(theta[7])+(((cos(theta[1])*cos(theta[2])*cos(theta[3])-sin(theta[1])*sin(theta[3]))*cos(theta[4])-cos(theta[1])*sin(theta[2])*sin(theta[4]))*
                sin(theta[5])+(cos(theta[1])*cos(theta[2])*sin(theta[3])+sin(theta[1])*cos(theta[3]))*cos(theta[5]))*cos(theta[7]))*cos(theta[8]))*r21+(((
                                                                                                                                                             ((cos(theta[1])*cos(theta[2])*cos(theta[3])-sin(theta[1])*sin(theta[3]))*sin(theta[4])+cos(theta[1])*sin(theta[2])*cos(theta[4]))*sin(theta[6])+(
                    (cos(theta[1])*cos(theta[2])*sin(theta[3])+sin(theta[1])*cos(theta[3]))*sin(theta[5])+
                (cos(theta[1])*sin(theta[2])*sin(theta[4])+(sin(theta[1])*sin(theta[3])-cos(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*cos(theta[5]))*cos(theta[6])
                )*sin(theta[7])+((cos(theta[1])*sin(theta[2])*sin(theta[4])+(sin(theta[1])*sin(theta[3])-cos(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*
                sin(theta[5])+(-cos(theta[1])*cos(theta[2])*sin(theta[3])-sin(theta[1])*cos(theta[3]))*cos(theta[5]))*cos(theta[7]))*sin(theta[8])+((
                                                                                                                                                        ((cos(theta[1])*sin(theta[3])+sin(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[4])+sin(theta[1])*sin(theta[2])*cos(theta[4]))*sin(theta[6])+(
                    (sin(theta[1])*cos(theta[2])*sin(theta[3])-cos(theta[1])*cos(theta[3]))*sin(theta[5])+
                (sin(theta[1])*sin(theta[2])*sin(theta[4])+(-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*cos(theta[5]))*
                cos(theta[6]))*sin(theta[7])+(
                    (sin(theta[1])*sin(theta[2])*sin(theta[4])+(-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*sin(theta[5])+
                (cos(theta[1])*cos(theta[3])-sin(theta[1])*cos(theta[2])*sin(theta[3]))*cos(theta[5]))*cos(theta[7]))*cos(theta[8]))*r11)/(r21*r21+r11*r11);

        Jw.at<double>(1,7) = (((((cos(theta[1])*sin(theta[2])*sin(theta[4])+(sin(theta[1])*sin(theta[3])-cos(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*sin(theta[5])
                +(-cos(theta[1])*cos(theta[2])*sin(theta[3])-sin(theta[1])*cos(theta[3]))*cos(theta[5]))*sin(theta[7])+(
                    ((sin(theta[1])*sin(theta[3])-cos(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[4])-cos(theta[1])*sin(theta[2])*cos(theta[4]))*sin(theta[6])+(
                    (-cos(theta[1])*cos(theta[2])*sin(theta[3])-sin(theta[1])*cos(theta[3]))*sin(theta[5])+
                ((cos(theta[1])*cos(theta[2])*cos(theta[3])-sin(theta[1])*sin(theta[3]))*cos(theta[4])-cos(theta[1])*sin(theta[2])*sin(theta[4]))*cos(theta[5]))*cos(theta[6])
                )*cos(theta[7]))*sin(theta[8])+((
                                                    (sin(theta[1])*sin(theta[2])*sin(theta[4])+(-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*sin(theta[5])+
                (cos(theta[1])*cos(theta[3])-sin(theta[1])*cos(theta[2])*sin(theta[3]))*cos(theta[5]))*sin(theta[7])+(
                    ((-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[4])-sin(theta[1])*sin(theta[2])*cos(theta[4]))*sin(theta[6])+(
                    (cos(theta[1])*cos(theta[3])-sin(theta[1])*cos(theta[2])*sin(theta[3]))*sin(theta[5])+
                ((cos(theta[1])*sin(theta[3])+sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4])-sin(theta[1])*sin(theta[2])*sin(theta[4]))*cos(theta[5]))*cos(theta[6])
                )*cos(theta[7]))*cos(theta[8]))*r21+r11*r11)/(r21*r21+r11*r11);

        Jw.at<double>(1,8) = 0.0;

        Jw.at<double>(1,9) = 0.0;


        //----------------------

        Jw.at<double>(2,0) = 0;

        Jw.at<double>(2,1) = -((((cos(theta[2])*cos(theta[3])*sin(theta[4])+sin(theta[2])*cos(theta[4]))*sin(theta[6])+
                (cos(theta[2])*sin(theta[3])*sin(theta[5])+(sin(theta[2])*sin(theta[4])-cos(theta[2])*cos(theta[3])*cos(theta[4]))*cos(theta[5]))*cos(theta[6]))*sin(theta[7])
                +((sin(theta[2])*sin(theta[4])-cos(theta[2])*cos(theta[3])*cos(theta[4]))*sin(theta[5])-cos(theta[2])*sin(theta[3])*cos(theta[5]))*cos(theta[7]))*r33+(
                    (cos(theta[2])*sin(theta[3])*sin(theta[5])+(sin(theta[2])*sin(theta[4])-cos(theta[2])*cos(theta[3])*cos(theta[4]))*cos(theta[5]))*sin(theta[6])+
                (-cos(theta[2])*cos(theta[3])*sin(theta[4])-sin(theta[2])*cos(theta[4]))*cos(theta[6]))*r32)/(r33*r33+r32*r32);


        Jw.at<double>(2,2) = (((sin(theta[2])*sin(theta[3])*sin(theta[4])*sin(theta[6])+
                (-sin(theta[2])*cos(theta[3])*sin(theta[5])-sin(theta[2])*sin(theta[3])*cos(theta[4])*cos(theta[5]))*cos(theta[6]))*sin(theta[7])+
                (sin(theta[2])*cos(theta[3])*cos(theta[5])-sin(theta[2])*sin(theta[3])*cos(theta[4])*sin(theta[5]))*cos(theta[7]))*r33+(
                    (-sin(theta[2])*cos(theta[3])*sin(theta[5])-sin(theta[2])*sin(theta[3])*cos(theta[4])*cos(theta[5]))*sin(theta[6])-sin(theta[2])*sin(theta[3])*sin(theta[4])*
                cos(theta[6]))*r32)/(r33*r33+r32*r32);

        Jw.at<double>(2,3) = -((((cos(theta[2])*sin(theta[4])+sin(theta[2])*cos(theta[3])*cos(theta[4]))*sin(theta[6])+
                (sin(theta[2])*cos(theta[3])*sin(theta[4])-cos(theta[2])*cos(theta[4]))*cos(theta[5])*cos(theta[6]))*sin(theta[7])+
                (sin(theta[2])*cos(theta[3])*sin(theta[4])-cos(theta[2])*cos(theta[4]))*sin(theta[5])*cos(theta[7]))*r33+(
                    (sin(theta[2])*cos(theta[3])*sin(theta[4])-cos(theta[2])*cos(theta[4]))*cos(theta[5])*sin(theta[6])+
                (-cos(theta[2])*sin(theta[4])-sin(theta[2])*cos(theta[3])*cos(theta[4]))*cos(theta[6]))*r32)/(r33*r33+r32*r32);

        Jw.at<double>(2,4) = -((((cos(theta[2])*sin(theta[4])+sin(theta[2])*cos(theta[3])*cos(theta[4]))*sin(theta[5])+sin(theta[2])*sin(theta[3])*cos(theta[5]))*cos(theta[6])*
                sin(theta[7])+(sin(theta[2])*sin(theta[3])*sin(theta[5])+(-cos(theta[2])*sin(theta[4])-sin(theta[2])*cos(theta[3])*cos(theta[4]))*cos(theta[5]))*
                cos(theta[7]))*r33+((cos(theta[2])*sin(theta[4])+sin(theta[2])*cos(theta[3])*cos(theta[4]))*sin(theta[5])+sin(theta[2])*sin(theta[3])*cos(theta[5]))*
                sin(theta[6])*r32)/(r33*r33+r32*r32);

        Jw.at<double>(2,5) = (((sin(theta[2])*sin(theta[3])*sin(theta[5])+(-cos(theta[2])*sin(theta[4])-sin(theta[2])*cos(theta[3])*cos(theta[4]))*cos(theta[5]))*sin(theta[6])+
                (cos(theta[2])*cos(theta[4])-sin(theta[2])*cos(theta[3])*sin(theta[4]))*cos(theta[6]))*sin(theta[7])*r33+(
                    (cos(theta[2])*cos(theta[4])-sin(theta[2])*cos(theta[3])*sin(theta[4]))*sin(theta[6])+
                ((cos(theta[2])*sin(theta[4])+sin(theta[2])*cos(theta[3])*cos(theta[4]))*cos(theta[5])-sin(theta[2])*sin(theta[3])*sin(theta[5]))*cos(theta[6]))*r32)/(r33*r33
                                                                                                                                                                       +r32*r32);

        Jw.at<double>(2,6) = -((((cos(theta[2])*sin(theta[4])+sin(theta[2])*cos(theta[3])*cos(theta[4]))*sin(theta[5])+sin(theta[2])*sin(theta[3])*cos(theta[5]))*sin(theta[7])+
                ((sin(theta[2])*cos(theta[3])*sin(theta[4])-cos(theta[2])*cos(theta[4]))*sin(theta[6])+
                (sin(theta[2])*sin(theta[3])*sin(theta[5])+(-cos(theta[2])*sin(theta[4])-sin(theta[2])*cos(theta[3])*cos(theta[4]))*cos(theta[5]))*cos(theta[6]))*
                cos(theta[7]))*r33)/(r33*r33+r32*r32);

        Jw.at<double>(2,7) = 0.0;

        Jw.at<double>(2,8) = 0.0;

        Jw.at<double>(2,9) = 0.0;

    }
    else
    {
        cout << "Beta must lie between -pi/2 to +pi/2" << endl;
        exit(-1);

    }
}
//=======================================================

//=================================================
// Computes the coordinates after rotation given in roll-pitch-yaw
// xp[3][3] - original coordinate axes
// xp2[3][3] - coordinate axes after rotation
// rpy[3] - roll-pitch-yaw angles in RADIAN
// t[3][3] - translation matrix
// xp2 = Rz * Ry * Rx * xp + t
//----------------------------------------------------------------
void rotated_axis(const cv::Mat &XP, const double rpy[], const cv::Mat &T, cv::Mat &XP2)
{

    double Alpha = rpy[0]; // roll - rotation about z axis
    double Beta = rpy[1]; // Pitch - rotation about y-axis
    double Gamma = rpy[2]; // Yaw - rotation about x-axis

    //Rotation about z axis
    double rz[3][3] = {
        {cos(Alpha), -sin(Alpha), 0},
        {sin(Alpha), cos(Alpha), 0},
        {0, 0, 1}
    };
    Mat RZ = Mat(3,3,CV_64F, &rz);

    // Rotation about y axis
    double ry[3][3] = {
        {cos(Beta), 0, sin(Beta)},
        {0, 1, 0},
        {-sin(Beta), 0, cos(Beta)}
    };
    Mat RY = Mat(3,3,CV_64F, &ry);


    // Rotation about X axis
    double rx[3][3] = {
        {1, 0, 0},
        {0, cos(Gamma), -sin(Gamma)},
        {0, sin(Gamma), cos(Gamma)}
    };
    Mat RX = Mat(3,3,CV_64F, &rx);


    // Transformed axes

    XP2 = RZ * RY * RX * XP.t() + T;

}







