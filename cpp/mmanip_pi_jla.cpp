/* Pseudo-inverse based Inverse Kinematic Solution
 * Use Null-space joint velocity for JOINT Limit Avoidance (JLA)
 *
 * Method I:
 * \dot{q} = pinv(J) * (\dot{x}+ Kp * error) + (I-pinv(J)*J)\dot{q}_0
 *
 * Method II:
 * \dot{q} = pinv(J) * error + (I-pinv(J)*J)\dot{q}_0
 *
 * \dot{q}_0 = alpha * dW/dq
 * where W(q) avoid joint limits
 *
 * Robot: GWAM from Barret
 *
 * Date: January 29, 2016
 *
 * Status: Both methods I and II are working perfectly. I tried several points. Change the target pose by changing theta_t variable.
 *
 * -----------------------------------------
*/

#include<iostream>
#include<mobile_manip.h>
#include<fstream>
#include<gnuplot_ci.h>

using namespace std;
using namespace gnuplot_ci;





int main()
{
    // ---------------------------------------------------------------
    //Desired Target Pose

    double theta_t[NL] = {DEG2RAD(20), DEG2RAD(50), DEG2RAD(30), DEG2RAD(50), DEG2RAD(0), DEG2RAD(30), DEG2RAD(20), DEG2RAD(10), 1, 1};
    double pose_t[NW];
    bool VALID_FLAG;

    mmanip_pose_fk(theta_t, pose_t, VALID_FLAG);

    cv::Mat Pose_T(6,1, CV_64F, pose_t);



    //Desired Robot Configuration (pose)
    cv::Mat Jpos_t(NR,3,CV_64F,0.0);
    joint_position(theta_t, Jpos_t);



    ofstream f7("target_pose.txt");
    for(int i = 0; i < 6; ++i)
        f7 << pose_t[i] << "\t" ;
    f7 << endl;
    f7.close();

    ofstream f8("target_config.txt");

    for(int i = 0; i < NR; ++i)
    {
        for(int j = 0; j < 3; ++j)
            f8 << Jpos_t.at<double>(i,j) << "\t";
        f8 << endl;
    }
    f8.close();
    //------------------------------------------

    // Initial values

    double pose[NW];
    double theta[NL] = {0,0,0,0,0,0,0,0,0,0};

    mmanip_pose_fk(theta, pose, VALID_FLAG);

    cv::Mat Pose_C(6,1, CV_64F, pose);

    //Initial Robot Config
    cv::Mat Jpos(NR,3,CV_64F,0.0);
    joint_position(theta, Jpos);

    ofstream f9("init_config.txt");
    for(int i = 0; i < NR; ++i)
    {
        for(int j = 0; j < 3; ++j)
            f9 << Jpos.at<double>(i,j) << "\t";
        f9 << endl;
    }
    f9.close();
    //-----------------------------------------------------


    cv::Mat Pose_T_dot(NW,1, CV_64F, 0.0);
    cv::Mat Jp(NC,NL,CV_64F, 0.0);
    cv::Mat Jw(3,NL,CV_64F, 0.0);
    cv::Mat J(NW,NL,CV_64F, 0.0);
    cv::Mat Theta_dot(NL,1,CV_64F, 0.0);
    cv::Mat Error(NW,1,CV_64F, 0.0);
    cv::Mat Jpinv(NL,NW, CV_64F, 0.0);
    cv::Mat singval(NW,1,CV_64F,0.0); // singular values
    cv::Mat q0_dot(NL,1,CV_64F,0.0); // Self-motion velocity

    // Gains
    cv::Mat Kp(NW,NW,CV_64F,0.0);
    Kp = 5 * cv::Mat::eye(Kp.rows, Kp.cols, CV_64F);




    // Control Loop

    ofstream f1("actual.txt");
    ofstream f2("rconfig.txt");
    ofstream f3("rank.txt");
    ofstream f5("error.txt");
    int Tmax = 20;
    double dt = 0.001;
    for(double t = 0; t < Tmax; t = t + dt)
    {

        Error = Pose_T - Pose_C;

        //Jacobian
        position_jacobian(theta, Jp);
        orientation_jacobian(theta, Jw);
        cv::vconcat(Jp, Jw, J);

        cv::SVD::compute(J, singval, cv::SVD::NO_UV);

        double rank = 0;
        for(int i = 0; i < NL; ++i)
        {
            if(singval.at<double>(i,1) > 0.0001)
                rank = rank + 1;
        }
        f3 << rank << endl;


        cv::invert(J, Jpinv, cv::DECOMP_SVD);



        // Here w(q) = 1/n* \sum_i{(theta[i]/(theta_max[i] - theta_min[i]))^2}
        // dw/dq_i = theta[i];
        // null space optimization for obtained a desired joint configuration
        for(int i = 0; i < NL; ++i)
        {
            q0_dot.at<double>(i,0) = 2*theta[i] / (NL * pow((theta_max[i] - theta_min[i]),2.0));
        }


        // Joint angle velocity using Self-motion component
        // pseudoinverse solution with nullspace optimization
        //Theta_dot = Jpinv * (Pose_T_dot + Kp*Error) - 1.5*(cv::Mat::eye(NL,NL,CV_64F)-Jpinv*J) * q0_dot;


        // Pseudo-inverse with Null Space Optimzation (NSO) = PI+NSO
        Theta_dot = Jpinv * Error - (cv::Mat::eye(NL,NL,CV_64F)-Jpinv*J) * q0_dot;

        for(int i = 0; i < NL; ++i)
        {
            theta[i] = theta[i] + dt * Theta_dot.at<double>(i);

//            if(theta[i] > theta_max[i]) theta[i] = theta_max[i];
//            else if(theta[i] < theta_min[i]) theta[i] = theta_min[i];
        }



        mmanip_pose_fk(theta, pose, VALID_FLAG);
        joint_position(theta, Jpos);

        for(int i = 0; i < NR; ++i)
        {
            for(int j = 0; j < 3; ++j)
                f2 << Jpos.at<double>(i,j) << "\t";
            f2 << endl;
        }
        f2 << endl << endl;

        if(VALID_FLAG)
        {
            for(int i = 0; i < NW; ++i)
            {
                Pose_C.at<double>(i) = pose[i];
                f1 << Pose_C.at<double>(i) << "\t";
            }
            f1 << endl;
        }

         f5 << t << "\t" << sqrt(cv::Mat(Error.t()*Error).at<double>(0,0)/6.0)<< endl;
    } // time-loop
    f1.close();
    f2.close();
    f3.close();
    f5.close();


    // ----------------------
    // Final Pose
    ofstream f6("final_config.txt");
    for(int i = 0; i < NR; ++i)
    {
        for(int j = 0; j < 3; ++j)
            f6 << Jpos.at<double>(i,j) << "\t";
        f6 << endl;
    }
    f6.close();


    //---------------------------
    // Coordinate Frames
    //---------------------------

    double dx = 0.3, dx_t = 0.2;
    double xorg[3] = {0,0,0};
    double xb[3][3] = {{xorg[0]+dx,0,0}, {0,xorg[1]+dx,0}, {0,0,xorg[2]+dx} };
    double xb_t[3][3] = {{xorg[0]+dx_t,0,0}, {0,xorg[1]+dx_t,0}, {0,0,xorg[2]+dx_t} };

    double xend[3][3] = {{pose[0], pose[1], pose[2]},
                         {pose[0], pose[1], pose[2]},
                         {pose[0], pose[1], pose[2]}};

    double xend_t[3][3] = {{pose_t[0], pose_t[1], pose_t[2]},
                          {pose_t[0], pose_t[1], pose_t[2]},
                          {pose_t[0], pose_t[1], pose_t[2]}};


    double xeb[3][3], xeb_t[3][3];

    cv::Mat R(3,3,CV_64F,0.0); // rotation matrix for final theta
    cv::Mat R_t(3,3,CV_64F,0.0); //rotation matrix for target pose
    cv::Mat T(3,3,CV_64F, &xend);
    cv::Mat T2(3,3,CV_64F, &xend_t);
    cv::Mat baseFrame(3,3,CV_64F, &xb);
    cv::Mat baseFrame_t(3,3,CV_64F, &xb_t); // used for target_pose
    cv::Mat endFrame(3,3,CV_64F, 0.0);
    cv::Mat endFrame_t(3,3,CV_64F, 0.0);

    rotation_matrix(theta, R);
    rotation_matrix(theta_t, R_t);

    // output is a column matrix
    endFrame = R * baseFrame.t() + T.t(); // check the transpose
    endFrame_t = R_t * baseFrame_t.t() + T2.t(); // check the transpose

    // make it a row matrix
    for(int i = 0; i < 3; ++i)
    {
        for(int j = 0; j < 3; ++j)
        {
            xeb[i][j] = endFrame.at<double>(j,i); // make it a row matrix
            xeb_t[i][j] = endFrame_t.at<double>(j,i); // make it a row
        }
    }

    // ---------------------
    // Plotting
    // -----------------


    GP_handle G1("/usr/bin/", "X (m)", "Y (m)", "Z (m)");
    G1.gnuplot_cmd("set terminal wxt");
    G1.gnuplot_cmd("set border");
    G1.gnuplot_cmd("splot 'actual.txt' u 1:2:3 w p t 'actual', 'target_pose.txt' u 1:2:3 w p ps 3  t 'target'");

    GP_handle G2("/usr/bin/", "Time (s)", "Error");
    G2.gnuplot_cmd("set terminal wxt");
    G2.gnuplot_cmd("set border");
    G2.gnuplot_cmd("plot 'error.txt' u 1:2 w l");


    GP_handle G3("/usr/bin/", "Rank", "Time (s)");
    G3.gnuplot_cmd("set terminal wxt");
    G3.gnuplot_cmd("set border");
    G3.gnuplot_cmd("plot 'rank.txt' w l");


    GP_handle G4("/usr/bin/", "X (m)", "Y (m)", "Z(m)");
    G4.gnuplot_cmd("set terminal wxt");
    G4.gnuplot_cmd("set border");
    G4.gnuplot_cmd("set ticslevel 0");
    G4.gnuplot_cmd("splot 'final_config.txt' u 1:2:3 w lp lw 2 t 'actual', 'init_config.txt' u 1:2:3 w lp t 'Init', 'target_config.txt' u 1:2:3 w lp t 'target'");
    G4.gnuplot_cmd("replot 'actual.txt' u 1:2:3 w d t 'EE trajectory'");
    G4.draw3dcoordAxis(xorg, xb[0], xb[1], xb[2],true);
    G4.draw3dcoordAxis(xend[0], xeb[0], xeb[1], xeb[2], true, 1);
    G4.draw3dcoordAxis(xend_t[0], xeb_t[0], xeb_t[1], xeb_t[2], true, 2);



     cout << "Press Enter to exit .." << endl;
    getchar();


    return 0;
}
