/* Jacobian Transpose Method For solving IK for a given Robot POSE (position + orientation)
 *
 * Robot: GWAM
 *
 * Date: January 29, 2016
 *
 * Status: Working ...
 *
 * -----------------------------------------
*/

#include<iostream>
#include<gwam_fk.h>
#include<fstream>
#include<gnuplot_ci.h>

using namespace std;
using namespace gnuplot_ci;

const int NC = 3;




int main()
{

    //Desired Target Pose

    double theta_t[7] = {DEG2RAD(30), DEG2RAD(50), DEG2RAD(20), DEG2RAD(50), DEG2RAD(10), DEG2RAD(20), DEG2RAD(0)};
    double pose_t[6];
    bool VALID_FLAG;
    gwam_pose_fk(theta_t, pose_t, VALID_FLAG);

    // Initial values
    double pose[6];
    double theta[NL] = {0,0,0,0,0,0,0};
    gwam_pose_fk(theta, pose, VALID_FLAG);


    cv::Mat Pose_T(6,1, CV_64F, pose_t);
    cv::Mat Pose_C(6,1, CV_64F, pose);

    cv::Mat Jp(3,7,CV_64F, 0.0);
    cv::Mat Jw(3,7,CV_64F, 0.0);
    cv::Mat J(6,7,CV_64F, 0.0);

    cv::Mat Theta_dot(7,1,CV_64F, 0.0);
    cv::Mat Error(6,1,CV_64F, 0.0);

    cv::Mat Jpos(5,3,CV_64F,0.0);
    cv::Mat Jpos2(5,3,CV_64F,0.0);


    ofstream f1("actual.txt");
    ofstream f2("rconfig.txt");

    ofstream f5("error.txt");

    int Tmax = 100;
    double dt = 0.01;
    for(double t = 0; t < Tmax; t = t + dt)
    {
        // Pose Error
        Error = Pose_T - Pose_C;
        f5 << t << "\t" << sqrt(cv::Mat(Error.t()*Error).at<double>(0,0)/6.0)<< endl;

        //Jacobian
        position_jacobian(theta, Jp);
        orientation_jacobian(theta, Jw);
        cv::vconcat(Jp, Jw, J);


        //step size : alpha
        double alpha = cv::norm(J.t()*Error) / cv::norm(J*J.t()*Error);

        // Jacobian Transpose Method for computing theta_dot
        Theta_dot = alpha * J.t() * Error;


        for(int i = 0; i < NL; ++i)
            theta[i] = theta[i] + dt * Theta_dot.at<double>(i);

        gwam_pose_fk(theta, pose, VALID_FLAG);
        joint_position(theta, Jpos);

        for(int i = 0; i < 5; ++i)
        {
            for(int j = 0; j < 3; ++j)
                f2 << Jpos.at<double>(i,j) << "\t";
            f2 << endl;
        }
        f2 << endl << endl;

        //update current pose

        if(VALID_FLAG)
        {
            for(int i = 0; i < 6; ++i)
            {
                Pose_C.at<double>(i) = pose[i];
                f1 << Pose_C.at<double>(i) << "\t";
            }
            f1 << endl;
        }


    } // time-loop
    f1.close();
    f2.close();

    f5.close();


    // Save the last pose and target pose.


    joint_position(theta_t, Jpos2);

    ofstream f6("final_pose.txt");

    for(int i = 0; i < 5; ++i)
    {
        for(int j = 0; j < 3; ++j)
            f6 << Jpos.at<double>(i,j) << "\t";
        for(int j = 0; j < 3; ++j)
            f6 << Jpos2.at<double>(i,j) << "\t";
        f6 << endl;
    }
    f6.close();

    //------------------
     ofstream f4("target.txt");

     for(int i = 0; i < 6; ++i)
         f4 << pose[i] << "\t";
     for(int i = 0; i < 6; ++i)
         f4 << pose_t[i] << "\t";
     f4 << endl;

     f4.close();




    // ---------------------
    // Plotting
    // -----------------


    GP_handle G1("/usr/bin/", "X (m)", "Y (m)", "Z (m)");
    G1.gnuplot_cmd("set terminal wxt");
    G1.gnuplot_cmd("set border");
    G1.gnuplot_cmd("splot 'actual.txt' u 1:2:3 w p t 'actual', 'target.txt' u 1:2:3 w p ps 4");



    GP_handle G2("/usr/bin/", "Time (s)", "Error");
    G2.gnuplot_cmd("set terminal wxt");
    G2.gnuplot_cmd("set border");
    G2.gnuplot_cmd("plot 'error.txt' u 1:2 w l t 'pose error'");


    double xorg[3] = {0,0,0};
    double xb[3][3] = {{0.3,0,0}, {0,0.3,0}, {0,0,0.3}};
    double xb2[3][3] = {{0.2,0,0}, {0,0.2,0}, {0,0,0.2}};


    GP_handle G4("/usr/bin/", "X(m)", "Y(m)", "Z(m)");

    G4.gnuplot_cmd("set terminal wxt");
    G4.gnuplot_cmd("set border");
    G4.gnuplot_cmd("set ticslevel 0");
    //G4.gnuplot_cmd("set yrange [-0.2:0.2]");
    G4.gnuplot_cmd("splot 'final_pose.txt' u 1:2:3 w lp t 'actual', '' u 4:5:6 w lp t 'desired', 'target.txt' u 1:2:3 w p ps 3");
    G4.draw3dcoordAxis(xorg, xb[0], xb[1], xb[2],true);


    double xend[3][3] = {{pose[0], pose[1], pose[2]},
                         {pose[0], pose[1], pose[2]},
                         {pose[0], pose[1], pose[2]}};

    double xend2[3][3] = {{pose_t[0], pose_t[1], pose_t[2]},
                          {pose_t[0], pose_t[1], pose_t[2]},
                          {pose_t[0], pose_t[1], pose_t[2]}};


    double xeb[3][3], xeb2[3][3];


    cv::Mat R(3,3,CV_64F,0.0);
    cv::Mat R2(3,3,CV_64F,0.0);
    cv::Mat T(3,3,CV_64F, &xend);
    cv::Mat T2(3,3,CV_64F, &xend2);
    cv::Mat baseC(3,3,CV_64F, &xb);
    cv::Mat baseC2(3,3,CV_64F, &xb2);
    cv::Mat endC(3,3,CV_64F, 0.0);
    cv::Mat endC2(3,3,CV_64F, 0.0);

    rotation_matrix(theta, R);
    rotation_matrix(theta_t, R2);

    // output is a column matrix
    endC = R * baseC.t() + T.t(); // check the transpose
    endC2 = R2 * baseC2.t() + T2.t(); // check the transpose

    // make it a row matrix
    for(int i = 0; i < 3; ++i)
    {
        for(int j = 0; j < 3; ++j)
        {
            xeb[i][j] = endC.at<double>(j,i); // make it a row matrix
            xeb2[i][j] = endC2.at<double>(j,i); // make it a row
        }
    }


    G4.draw3dcoordAxis(xend[0], xeb[0], xeb[1], xeb[2], true, 1);
    G4.draw3dcoordAxis(xend2[0], xeb2[0], xeb2[1], xeb2[2], true, 2);


    getchar();



    return 0;
}
