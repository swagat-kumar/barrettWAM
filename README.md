This package provides programs for solving inverse kinematics for
Barrets WAM Robotics ARM. 

Author: Swagat Kumar

Email: swagat.kumar@gmail.com

License: GPL 2.0 or above

---------------------------------------------------------

Folders:

cpp/ : contains C++ source codes

maxima/: Contains maxima file for generating Forward Kinematics
Equations


Dependencies:
- Gnuplot C++ interface library: URL:https://gitlab.com/swagat-kumar/gnuplot_ci.git
- OpenCV
- GNU/Linux (Tested on Ubuntu 14.04 LTS x86 System)


Compilation Information:

- Run the CMake File available within the cpp folder. 

Content
- Forward Kinematics with pose
- Inverse Kinematics using Pseudo-inverse, Jacobian Transpose Method
and PD controller
- Jacobian Computation using two methods (Euler Angle and Angular
    Velocity)
- C++ Class Module


